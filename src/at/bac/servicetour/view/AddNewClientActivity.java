/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.controller.IClientController;
import at.bac.servicetour.domain.ClientDomain;
import at.bac.servicetour.model.MapFunctions;

/** Add new client activity
 * 
 * This activity gives the user the opportunity to create a new appointment.
 * The requested fields has to be filled out and can be saved by pressing the button.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class AddNewClientActivity extends Activity implements OnClickListener {

	private static final String TAG = AddNewClientActivity.class.getSimpleName();
	
	// Activity elements
	private Button buttonSave; 
	private Button showAddress;
	
	// Controller for DB
	private IClientController cC = new ClientController();
	private MapFunctions map = new MapFunctions();
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clientaddnewclient);
        
        buttonSave 	= 	(Button) findViewById(R.id.add_new_client_button_save);
        buttonSave.setOnClickListener(this);
        
        showAddress = (Button) findViewById(R.id.add_new_client_button_showaddress);
        showAddress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				EditText et = (EditText) findViewById(R.id.add_new_client_editText_street);
				String street = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_housenumber);
				String houseNumber = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_city);
				String city = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_zip);
				String zip = et.getText().toString().trim();
				
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
				Uri.parse("geo:0,0?q=" + street.replace(" ", "+") + "+" + houseNumber.replace(" ", "+") + "+" + zip.replace(" ", "+") + "+" + city.replace(" ", "+")));
				startActivity(intent);

			}

		});
	}

	/** On Click Listener
	 * 
	 * If a button is pressed, this method is called
	 * 
	 * @param v The view which calls this method
	 * 
	 */
	public void onClick(View v) {
		
		switch(v.getId()) {
		case R.id.add_new_client_button_save: 
			
			boolean canSave = true;
			
			buttonSave.setEnabled(false);
			
			EditText et = (EditText) findViewById(R.id.add_new_client_editText_street);
			String street = et.getText().toString().trim();
			et = (EditText) findViewById(R.id.add_new_client_editText_housenumber);
			String houseNumber = et.getText().toString().trim();
			et = (EditText) findViewById(R.id.add_new_client_editText_city);
			String city = et.getText().toString().trim();
			et = (EditText) findViewById(R.id.add_new_client_editText_zip);
			int zip = 0;
			boolean zipIsEmpty = false;
			try {
				if (et.getText().toString().trim().equals("")) {
					zipIsEmpty = true;
				} else {
					zip = Integer.valueOf(et.getText().toString().trim());
				}
			} catch (NumberFormatException e) {
				canSave = false;

				AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
				alertbox.setMessage(getResources().getString(
						R.string.add_new_client_text_zipmustbeint));
				alertbox.setNeutralButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface arg0,
									int arg1) {
							}
						});
				alertbox.show();
				
				buttonSave.setEnabled(true);
				
				//e.printStackTrace();
			}
			
			
			if (map.validateAddress(street.replace(" ", "+")+"+"+houseNumber.replace(" ", "+") + "+" + zip + "+" + city)) { // validates the address
				
				et = (EditText) findViewById(R.id.add_new_client_editText_firstname);
				String firstName = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_lastname);
				String lastName = et.getText().toString().trim();
				
				
				if (firstName.equals("") || lastName.equals("") // All required fields are filled out
						|| street.equals("") || houseNumber.equals("")
						|| zipIsEmpty || city.equals("")) {
					canSave = false;

					AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
					alertbox.setMessage(getResources().getString(
							R.string.add_new_client_text_noNameAndAdr));
					alertbox.setNeutralButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
								}
							});
					alertbox.show();
					buttonSave.setEnabled(true);
				}
				et = (EditText) findViewById(R.id.add_new_client_editText_phone);
				String phone = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_mobil);
				String mobil = et.getText().toString().trim();
				et = (EditText) findViewById(R.id.add_new_client_editText_note);
				String note = et.getText().toString().trim();
				if (canSave) {
					ClientDomain client = new ClientDomain();
					client.firstName = firstName;
					client.lastName = lastName;
					client.street = street;
					client.houseNumber = houseNumber;
					client.zip = zip;
					client.city = city;
					client.phone = phone;
					client.mobil = mobil;
					client.note = note;

					if (cC.createNewClient(client)) { // Saves the client
						Toast.makeText(this,
								R.string.add_new_client_toast_client_saved,
								Toast.LENGTH_LONG).show();
					} else {
						Log.e(TAG, "FAIL: Create Client");
					}

					finish();
				}
			} else {
				AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
				
	            alertbox.setMessage(getResources().getString(R.string.add_new_client_novailidaddress));
	 
	            alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
	 
	                public void onClick(DialogInterface arg0, int arg1) {

//	                    Toast.makeText(getApplicationContext(), "OK button clicked", Toast.LENGTH_LONG).show();
	                }
	            }).show();
	            
	            buttonSave.setEnabled(true);
	            
			}
			break;
		}
		
	}

}
