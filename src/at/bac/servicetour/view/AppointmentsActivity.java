/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.AppointmentDomain;

/** Appointments Activity
 * 
 * Displays the appointments of one day in a List
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class AppointmentsActivity extends Activity {

	private static final String TAG = AppointmentsActivity.class.getSimpleName();

	// DB Controller
	private AppointmentController aC = new AppointmentController();
	private ClientController cC = new ClientController();

	// HashMap to get the DB id of an appointment
	private HashMap<Long, String> appointmentHashMap;
	// The appointments of one day represented in a array-list
	private ArrayList<AppointmentDomain> appointments;

	// Activity elements
	private ListView lv;

	private Button rightButton;
	private Button leftButton;
	private Button goToDateButton;

	private TextView dayTextView;
	private TextView dateTextView;

	// The date representation
	private GregorianCalendar dateToShow;

	static final int DATE_DIALOG_ID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointments);

		dayTextView = (TextView) findViewById(R.id.appointments_textview_day);
		dateTextView = (TextView) findViewById(R.id.appointments_textview_date);

		rightButton = (Button) findViewById(R.id.appointments_button_goright);
		leftButton = (Button) findViewById(R.id.appointments_button_goleft);
		goToDateButton = (Button) findViewById(R.id.appointments_button_goToDate);

		lv = (ListView) findViewById(R.id.appointments_listview);
		lv.setTextFilterEnabled(true);

		lv.setOnItemClickListener(new OnItemClickListener() { // Listener to the List
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (id != -1) {
					startAppointmentDetailActivityIntent(appointmentHashMap
							.get(id));
				}
			}
		});

		rightButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				buildListForOtherDayDate(1);
			}

		});

		leftButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				buildListForOtherDayDate(-1);
			}

		});

		goToDateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showDialog(DATE_DIALOG_ID); 
			}

		});

		buildList(new GregorianCalendar()); // show today
	}

	/** Build list for an other day
	 * 
	 * Build a new list with an other date. The value i is used to show a date in the future or in the past.
	 * For example i=+1 = the next day, i=-1 the past day
	 * 
	 * @param i Added to the current date
	 */
	private void buildListForOtherDayDate(int i) {
		buildList(new GregorianCalendar(dateToShow.get(GregorianCalendar.YEAR), dateToShow.get(GregorianCalendar.MONTH), (dateToShow.get(GregorianCalendar.DAY_OF_MONTH)+i)));
	}

	/** Build List
	 * 
	 * Fills the list with the appointments of the requested date
	 * 
	 * @param dateToShow The requested date
	 */
	private void buildList(GregorianCalendar dateToShow) {

		this.dateToShow = dateToShow;

		String[] weekDays = getResources().getStringArray(R.array.weekdays_german); //The German weekdays
		this.dayTextView.setText(weekDays[dateToShow.get(GregorianCalendar.DAY_OF_WEEK)-1]);
		this.dateTextView.setText(dateToShow.get(GregorianCalendar.DAY_OF_MONTH) + "." + (dateToShow.get(GregorianCalendar.MONTH)+1) + " " + dateToShow.get(GregorianCalendar.YEAR));

		this.appointments = aC.getAllAppointmentsByDate(dateToShow); // Gets the appointments from the DB

		// List representation
		final String[] columns = { "Zeile1", "Zeile2" };
		final String[] matrix  = { "_id", "Zeile1", "Zeile2" };
		final int[] layouts = new int[] { android.R.id.text1, android.R.id.text2 };
		long key = 0;

		MatrixCursor cursor = new MatrixCursor(matrix);

		if(appointments.isEmpty()) { // If there are no appointments
			Log.d(TAG, "appointments isEmty true!");
			cursor.addRow(new Object[] {-1, getResources().getString(R.string.clients_text_noClientsinDB), ""});
		} else {
			this.appointmentHashMap = new HashMap<Long, String>();
			
			for(AppointmentDomain a : appointments) {

				key = key + 1;
				this.appointmentHashMap.put(key, a.id);

				String completed;
				if(a.completed) {
					completed = getResources().getString(R.string.appointments_listentry_completed);
				} else {
					completed = getResources().getString(R.string.appointments_listentry_notcompleted);
				}

				String isPrivate = "";
				if(a.isPrivate) isPrivate = " - " + getResources().getString(R.string.add_new_appointment_checkbox_private);

				cursor.addRow(new Object[] { // List entries are added
						key,
						cC.getClientByID(a.clientId).lastName + " " + cC.getClientByID(a.clientId).firstName,
						pad(a.from.get(GregorianCalendar.HOUR_OF_DAY)) + ":" + pad(a.from.get(GregorianCalendar.MINUTE)) + " - "
								+ pad(a.to.get(GregorianCalendar.HOUR_OF_DAY)) + ":" + pad(a.to.get(GregorianCalendar.MINUTE)) + " ("
								+ a.duration + "min)\n" + completed + isPrivate});
			}	
		}

		//for the layout, so the button don't block the last entry
		cursor.addRow(new Object[] {
				-1, "", ""});

		SimpleCursorAdapter data = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columns, layouts);

		lv.setAdapter(data);

	}

	/** Start Appointment Detail Activity
	 * 
	 * Starts the detail view of an appointment
	 * 
	 * @param id The id of the requested appointment
	 */
	private void startAppointmentDetailActivityIntent(String id) {
		Intent intent = new Intent(this, AppointmentDetailActivity.class); 
		intent.putExtra("id", id);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.appointmentsoptionmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		Intent intent;
		switch (item.getItemId()) {
		case R.id.appointments_optionmenu_newAppointment:
			intent = new Intent(this, AddNewAppointmentActivity.class);
			intent.putExtra("day", this.dateToShow.get(GregorianCalendar.DAY_OF_MONTH));
			intent.putExtra("month", this.dateToShow.get(GregorianCalendar.MONTH));
			intent.putExtra("year", this.dateToShow.get(GregorianCalendar.YEAR));
			intent.putExtra("setClient", false);
			startActivity(intent);
			return true;
		case R.id.appointments_optionmenu_calroute:

			if (!appointments.isEmpty()) {
				intent = new Intent(this, CalculateRouteActivity.class);
				intent.putExtra("day",
						this.dateToShow.get(GregorianCalendar.DAY_OF_MONTH));
				intent.putExtra("month",
						this.dateToShow.get(GregorianCalendar.MONTH));
				intent.putExtra("year",
						this.dateToShow.get(GregorianCalendar.YEAR));
				intent.putExtra(
						"textToShow",
						getResources().getString(
								R.string.calculate_route_textview_position));
				intent.putExtra("startedAsSubactivity", false);
				startActivity(intent);
			} else {
				Toast.makeText(this,
						R.string.appointments_toast_noappointments,
						Toast.LENGTH_LONG).show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this,
					mDateSetListener,
					this.dateToShow.get(Calendar.YEAR), this.dateToShow.get(Calendar.MONTH), this.dateToShow.get(Calendar.DAY_OF_MONTH));

		}
		return null;
	}

	// Date Picker Dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener =
			new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, 
				int monthOfYear, int dayOfMonth) {
			buildList(new GregorianCalendar(year, monthOfYear, dayOfMonth));
		}
	};


	@Override
	protected void onResume() {
		super.onResume();
		buildList(this.dateToShow);
	}
	
	
	/** Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front of the string representation.
	 * 
	 * @param c The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
}
