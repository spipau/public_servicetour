/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.controller.IAppointmentController;
import at.bac.servicetour.controller.IClientController;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/** Add New Appointment Activity
 * 
 * This activity gives the user the opportunity to create a new appointment.
 * The requested fields has to be filled out and can be saved by pressing the button.
 * The address is validated by google.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class AddNewAppointmentActivity extends Activity {

	// Activity elements
	private Button dateButton;
	private Button timetoButton;
	private Button timefromButton;
	private Button saveButton;
	private Button addClientButton;
	
	private TextView clientTextView;

	private EditText durationEditText;
	private EditText noteEditText;
	
	private CheckBox isPrivateCheckBox;
	
	// Date and time
	private int mYear;
	private int mMonth;
	private int mDay;
	private int mHourToday;
	private int mMinuteToday;
	private int mHourFrom;
	private int mMinuteFrom;
	private int mHourTo;
	private int mMinuteTo;

	// Id's for the dialogs
	static final int DATE_DIALOG_ID = 0;
	static final int TIME_DIALOG_FROM_ID = 1;
	static final int TIME_DIALOG_TO_ID = 2;
	static final int DIALOG_DURATION_ID = 3;
	
	// Id for the sub-activity
	static final int SUBACTIVITY_PICKCLIENT = 0;
	
	// Controller for the DB
	private IAppointmentController aC = new AppointmentController();
	private IClientController cC = new ClientController();

	// The client
	private ClientDomain client = new ClientDomain();

	// True if a client is selected, false if not
	private boolean isClientSet = false;
	
	private static final String TAG = AddNewAppointmentActivity.class.getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointmentaddnewappointment);

		dateButton = (Button) findViewById(R.id.add_new_appointment_button_date);
		timefromButton = (Button) findViewById(R.id.add_new_appointment_button_from);
		timetoButton = (Button) findViewById(R.id.add_new_appointment_button_to);
		saveButton = (Button) findViewById(R.id.add_new_appointment_button_save);
		addClientButton = (Button) findViewById(R.id.add_new_appointment_button_addclient);
		
		clientTextView = (TextView) findViewById(R.id.add_new_appointment_textview_client);

		durationEditText = (EditText) findViewById(R.id.add_new_appointment_edittext_duration);
		noteEditText = (EditText) findViewById(R.id.add_new_appointment_editText_note);
		
		isPrivateCheckBox = (CheckBox) findViewById(R.id.add_new_appointment_checkBox_private);
		
		Bundle map = getIntent().getExtras();
		
		if((Boolean) map.get("setClient")) { //new appointment or update
			this.client = cC.getClientByID(map.getString("clientID"));
			isClientSet = true;
		}
		
		int day = (Integer) map.get("day");
		int month = (Integer) map.get("month");
		int year = (Integer) map.get("year");		
		
		GregorianCalendar dateToShow = new GregorianCalendar(year, month, day); 
		mYear = dateToShow.get(Calendar.YEAR);
		mMonth = dateToShow.get(Calendar.MONTH);
		mDay = dateToShow.get(Calendar.DAY_OF_MONTH);
		
		final Calendar c = Calendar.getInstance();
		
//		Log.d(TAG, "!!!!!!!!!!!!!!!Date: " + mDay + "." + mMonth+1 + " " + mYear + " " + Calendar.DAY_OF_WEEK);
		
		mHourToday = c.get(Calendar.HOUR_OF_DAY);
		mHourFrom = mHourToday;
		mHourTo = (mHourToday % 24) + 1;
		if(mHourTo == 24) mHourTo = 00;
		mMinuteToday = c.get(Calendar.MINUTE);
		mMinuteFrom = mMinuteToday;
		mMinuteTo = mMinuteToday;


		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				saveAppointment();
			}

		});

		addClientButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startSubActivity(SUBACTIVITY_PICKCLIENT);
			}
		});
		
		dateButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		timefromButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_FROM_ID);
			}
		});

		timetoButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_TO_ID);
			}
		});

		updateDisplay();

	}

	/** Start sub-activity
	 * 
	 * Starts the PickClientActivity.java as a sub-activity
	 * 
	 * @param id The id of the desired sub-activity.
	 */
	private void startSubActivity(int id) {
		switch(id) {
		case SUBACTIVITY_PICKCLIENT:
			Intent intent = new Intent(this, PickClientActivity.class);
			intent.putExtra("SUBACTIVITY_PICKCLIENT", true);
			
			startActivityForResult(intent, SUBACTIVITY_PICKCLIENT);
			break;
		}
	}
	
	/** Update display
	 * 
	 * Builds the view of the activity.
	 * 
	 */
	private void updateDisplay() {
		 
		 String[] weekDays = getResources().getStringArray(R.array.weekdays_german);
		 
	    GregorianCalendar cal = new GregorianCalendar();
	    
	    cal.set( Calendar.DATE, mDay );
	    cal.set( Calendar.MONTH, mMonth );
	    cal.set( Calendar.YEAR, mYear );
		
		dateButton.setText( 
				new StringBuilder()
				// Month is 0 based so add 1
				.append(weekDays[cal.get(Calendar.DAY_OF_WEEK) -1]).append(" ")
				.append(mDay).append(".")
				.append(mMonth + 1).append(".")
				.append(mYear).append(" "));

		timefromButton.setText(
				new StringBuilder()
				.append(pad(mHourFrom)).append(":")
				.append(pad(mMinuteFrom)));

		timetoButton.setText(
				new StringBuilder()
				.append(pad(mHourTo)).append(":")
				.append(pad(mMinuteTo)));
		
		if(isClientSet) {
			clientTextView.setText(client.lastName + " " + client.firstName);
		}
		
		
	}

	// Listener for the date-picker
	private DatePickerDialog.OnDateSetListener mDateSetListener =
		new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, 
				int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	// Listener for the from time-picker
	private TimePickerDialog.OnTimeSetListener mTimeFromSetListener =
		new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {		
			
			mHourFrom = hourOfDay;
			mMinuteFrom = minute;
			mHourTo = (hourOfDay % 24) + 1;
			mMinuteTo = minute;
			updateDisplay();
		}
	};

	// Listener for the to time-picker
	private TimePickerDialog.OnTimeSetListener mTimeToSetListener =
		new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			
			if(mHourFrom <= hourOfDay) {				
				if(mHourFrom == hourOfDay) {				
					if (mMinuteFrom < minute) {				
						mHourTo = hourOfDay;
						mMinuteTo = minute;
						updateDisplay();
					} else {
						showErrorMessage(getResources().getString(R.string.add_new_appointment_text_wrongtotime));
					}
				} else {
					mHourTo = hourOfDay;
					mMinuteTo = minute;
					updateDisplay();
				}
			} else {
				showErrorMessage(getResources().getString(R.string.add_new_appointment_text_wrongtotime));
			}
		}
	};

	/** Show error message
	 * 
	 * Displays an error message with the desired text as an alert-dialog (ok).
	 * 
	 * @param message The error message text
	 */
	private void showErrorMessage(String message) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage(message);	 
		alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface arg0, int arg1) {   
			}
		});
		alertbox.show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this,
					mDateSetListener,
					mYear, mMonth, mDay);
		case TIME_DIALOG_FROM_ID:
			return new TimePickerDialog(this,
					mTimeFromSetListener, mHourToday, mMinuteToday, true); 
		case TIME_DIALOG_TO_ID:
			return new TimePickerDialog(this,
					mTimeToSetListener, mHourTo, mMinuteToday, true);

		}
		return null;
	}

	/** Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front of the string representation.
	 * 
	 * @param c The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	/** Save Appointment
	 * 
	 * Saves the appointment to the CouchDB
	 * 
	 */
	private void saveAppointment() {

		GregorianCalendar from = new GregorianCalendar();
		from.set(mYear, mMonth, mDay, mHourFrom, mMinuteFrom, 0);
		
		GregorianCalendar to = new GregorianCalendar();
		to.set(mYear, mMonth, mDay, mHourTo, mMinuteTo, 0);

		int duration = Integer.valueOf(durationEditText.getText().toString());
		String note = noteEditText.getText().toString();
		
		if(isClientSet) {
				if (duration <= ((mHourTo-mHourFrom)*60 + (mMinuteTo-mMinuteFrom))) {
					Log.d(TAG, "ClientID: " + this.client.id);
					AppointmentDomain appointment = new AppointmentDomain(from,
							to, duration, this.client.id, isPrivateCheckBox.isChecked(), note);
					if (aC.createAppointment(appointment)) {
						Toast.makeText(
								this,
								getResources()
										.getString(
												R.string.add_new_appointment_text_appointmentsaved),
								Toast.LENGTH_LONG).show();
						finish();
					} else {
						showErrorMessage(getResources().getString(
								R.string.add_new_appointment_text_savefail));
					}
				} else {
					showErrorMessage(getResources().getString(R.string.add_new_appointment_text_tolongduration));
				}

		} else {
			showErrorMessage(getResources().getString(R.string.add_new_appointment_text_pleaseaddcontact));
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
		case Activity.RESULT_OK:
			this.client = cC.getClientByID(data.getExtras().getString(PickClientActivity.CLIENT_ID));

			this.isClientSet = true;
			updateDisplay();
			break;
		case Activity.RESULT_CANCELED:
			Log.e(TAG, "FAIL: wrong callback from pickclient or back");
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
}
