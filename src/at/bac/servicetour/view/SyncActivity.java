/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.model.SyncCouchDBService;
import at.bac.servicetour.model.TsptwService;

/** Synchronization Activity
 * 
 * Displays the synchronization activity where the user can synchronizes his database with an server.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class SyncActivity extends Activity {

	private static final String TAG = SyncActivity.class.getSimpleName();

	// Activity elements
	private Button pushButton;
	private Button pullButton;

	private ProgressBar progressBarPush;
	private ProgressBar progressBarPull;

	// Preferences saved in the settings
	private SharedPreferences settings;

	// Binder for the Service
	private SyncCouchDBService.SyncCouchDBBinder syncBinder;

	private boolean pullServiceRunning = false;
	private boolean pushServiceRunning = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync);

		pushButton = (Button) findViewById(R.id.sync_button_push);
		pullButton = (Button) findViewById(R.id.sync_button_pull);

		progressBarPush = (ProgressBar) findViewById(R.id.sync_progressBarPush);
		progressBarPull = (ProgressBar) findViewById(R.id.sync_progressBarPull);

		settings = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);

		pushButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.i(TAG, "Push");
				pushButton.setVisibility(Button.INVISIBLE);
				pullButton.setVisibility(Button.INVISIBLE);
				progressBarPush.setVisibility(ProgressBar.VISIBLE);
				startPushService(); // Start service
			}

		});

		pullButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.i(TAG, "Pull");
				// starts the Service and bind it
				pullButton.setVisibility(Button.INVISIBLE);
				pushButton.setVisibility(Button.INVISIBLE);
				progressBarPull.setVisibility(ProgressBar.VISIBLE);
				startPullService(); // Start service
			}

		});

	}

	@Override
	protected void onStop() {
//		Log.i(TAG, "is S running: " + isMyServiceRunning());
		unbindService();
		super.onStop();
	}

	/** Start Pull Service
	 * 
	 * Starts a service for a pull synchronization.
	 * 
	 */
	private void startPullService() {
		pullServiceRunning = true;
		final Intent tsptwServiceIntent = new Intent(this, SyncCouchDBService.class);
		bindService(tsptwServiceIntent, mServiceConnectionPull, Context.BIND_AUTO_CREATE);
	}

	/** Start Push Service
	 * 
	 * Starts a service for a push synchronization.
	 * 
	 */
	private void startPushService() {
		pushServiceRunning = true;
		final Intent tsptwServiceIntent = new Intent(this, SyncCouchDBService.class);
		bindService(tsptwServiceIntent, mServiceConnectionPush, Context.BIND_AUTO_CREATE);
	}

	// Handler for the started service. Receives the message from the service.
	private final Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			final Bundle bundle = msg.getData();
			handleMessagefromService(bundle.getString("message"));

			super.handleMessage(msg);
		}
	};


	/** Handle Message from Service
	 * 
	 * Receives a message from the service and shows feedback to the user.
	 * 
	 * @param message The Message sent by the Service
	 */
	private void handleMessagefromService(String message) {
		unbindService();
		pushButton.setVisibility(Button.VISIBLE);
		pullButton.setVisibility(Button.VISIBLE);
		progressBarPull.setVisibility(ProgressBar.INVISIBLE);
		progressBarPush.setVisibility(ProgressBar.INVISIBLE);
		if(message.equals("ok")) {
			showToast();
		} else {
			showAlertDialog();
		}
	}

	// ServiceConnection to Service - onServiceConnected is called when Service is connected
	private ServiceConnection mServiceConnectionPull = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder binder) {
			syncBinder = (SyncCouchDBService.SyncCouchDBBinder) binder;
			syncBinder.setActivityCallbackHandler(messageHandler);
			syncBinder.setDataForSync(settings.getString("usernameSync", ""), settings.getString("pwSync", ""), settings.getString("serverSync", ""), settings.getString("dvSync", ""));

			syncBinder.pull();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			Log.i(TAG, "*********************\n\n\n\n\n\n\n\nSERVICE DISCONNECTED\n\n\n\n\n\n\n\n*********************");
		}
	};

	// ServiceConnection to Service - onServiceConnected is called when Service is connected
	private ServiceConnection mServiceConnectionPush = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder binder) {
			syncBinder = (SyncCouchDBService.SyncCouchDBBinder) binder;
			syncBinder.setActivityCallbackHandler(messageHandler);
			syncBinder.setDataForSync(settings.getString("usernameSync", ""), settings.getString("pwSync", ""), settings.getString("serverSync", ""), settings.getString("dvSync", ""));

			syncBinder.push();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			Log.i(TAG, "*********************\n\n\n\n\n\n\n\nSERVICE DISCONNECTED\n\n\n\n\n\n\n\n*********************");
		}
	};

	/** Show Alert Dialog
	 * 
	 * If the connection to the synchronization server wasn't possible a dialog is shown.
	 * 
	 */
	private void showAlertDialog() {
		new AlertDialog.Builder(this)
		.setMessage(getResources().getString(R.string.sync_alertdialog_message_failsync))
		.setTitle(getResources().getString(R.string.sync_alertdialog_message_title))
		.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {

			}
		})
		.show();
	}

	/** Show Toast
	 * 
	 * If the connection to the synchronization server was successful a toast message is shown.
	 * 
	 */
	private void showToast() {
		Toast.makeText(this,
				R.string.sync_toast_message,
				Toast.LENGTH_LONG).show();
	}

	/** Is my service running
	 * 
	 * Returns true if the sync service is running, otherwise false.
	 * 
	 * @return true if the service is running, otherwise false.
	 */
	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("at.bac.servicetour.model.SyncCouchDBService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/** Unbind Service
	 * 
	 * Unbinds a service if it's running.
	 * 
	 */
	private void unbindService() {
		if (isMyServiceRunning()) {
			if(pushServiceRunning) {
				unbindService(mServiceConnectionPush);
				stopService(new Intent(this, TsptwService.class));
				pushServiceRunning = false;
			} else if(pullServiceRunning) {
				unbindService(mServiceConnectionPull);
				stopService(new Intent(this, TsptwService.class));
				pullServiceRunning = false;
			}
		}
	}


	@Override
	public void onBackPressed() {
		unbindService();
		super.onBackPressed();
	}
}
