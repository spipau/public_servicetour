/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import at.bac.servicetour.R;

/** Main Activity
 * 
 * This activity is called when the CouchDB was started. It displays the main men�.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class MainActivity extends Activity implements OnClickListener {

	// Activity elements
	private Button buttonAppointments;
	private Button buttonClients;
	private Button buttonSync;
	private Button buttonSettings;
	private Button buttonExit;

	private static final String TAG = MainActivity.class.getSimpleName();
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);

		
		buttonAppointments = (Button) findViewById(R.id.main_button_appointments);
		buttonClients = (Button) findViewById(R.id.main_button_clients);
		buttonSync = (Button) findViewById(R.id.main_button_sync);
		buttonSettings = (Button) findViewById(R.id.main_button_settings);
		buttonExit = (Button) findViewById(R.id.main_button_exit);

		buttonAppointments.setOnClickListener(this);
		buttonClients.setOnClickListener(this);
		buttonSync.setOnClickListener(this);
		buttonSettings.setOnClickListener(this);
		buttonExit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.main_button_appointments:
			intent = new Intent(this, AppointmentsActivity.class);
			startActivity(intent);
			break;
		case R.id.main_button_clients:
			intent = new Intent(this, ClientsActivity.class);
			startActivity(intent);
			break;
		case R.id.main_button_sync:
			intent = new Intent(this, SyncActivity.class);
			startActivity(intent);
			break;
		case R.id.main_button_settings:
			intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			break;
		case R.id.main_button_exit:
			this.finish();
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	
}