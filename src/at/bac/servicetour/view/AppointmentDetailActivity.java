/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.controller.IAppointmentController;
import at.bac.servicetour.controller.IClientController;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/** Appointment Detail Activity
 * 
 * Displays the details of an appointment.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class AppointmentDetailActivity extends Activity {
	
	// Activity elements
	private Button goToClientButton;
	
	private TextView clientInfo;
	private TextView notes;
	private TextView timeFrom;
	private TextView timeTo;
	private TextView timeDuration;
	private TextView timeDate;
	
	private CheckBox completed;
	
	// The required id's and objects
	private AppointmentDomain appointment;
	private ClientDomain client;
	private String appointmentID;
	
	// The DB Controllers
	private IAppointmentController aC = new AppointmentController();
	private IClientController cC = new ClientController();
	
	private static final String TAG = AppointmentDetailActivity.class.getSimpleName();
	private static final int DIALOG_YES_NO_MESSAGE_DELETEAPPOINTMENT = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.appointmentdeatil);
		
		Bundle map = getIntent().getExtras();
		appointmentID = map.getString("id");
		
		goToClientButton = (Button) findViewById(R.id.appointment_detail_button_gotoclient);
		
		clientInfo = (TextView)findViewById(R.id.appointment_detail_textview_clientinfo);
		notes = (TextView)findViewById(R.id.appointment_detail_textview_notes);
		timeDate = (TextView)findViewById(R.id.appointment_detail_textview_date);
		timeFrom = (TextView)findViewById(R.id.appointment_detail_textview_from);
		timeTo = (TextView)findViewById(R.id.appointment_detail_textview_to);
		timeDuration = (TextView)findViewById(R.id.appointment_detail_textview_duration);
		completed = (CheckBox) findViewById(R.id.appointment_detail_checkbox_completed);
		
		goToClientButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startClientDetailActivityIntent(appointment.clientId);
			}

		});
		
		completed.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if (isChecked)
				{
					if(aC.updateAppointment(new AppointmentDomain(appointment.id, 
							appointment.rev, 
							appointment.from,
							appointment.to, 
							appointment.duration,
							appointment.clientId,
							appointment.isPrivate,
							appointment.note,
							true))) {	
						loadContent();
					} else {
						Log.e(TAG, "FAIL: update Appointment");
					}
				} else {
					if(aC.updateAppointment(new AppointmentDomain(appointment.id, 
							appointment.rev, 
							appointment.from,
							appointment.to, 
							appointment.duration,
							appointment.clientId,
							appointment.isPrivate,
							appointment.note,
							false))) {	
						loadContent();
					} else {
						Log.e(TAG, "FAIL: update Appointment");
					}
				}
			}
		});
		
		loadContent();
		
	}

	/** Load content
	 * 
	 * Loads and displays the content.
	 * 
	 */
    private void loadContent() {
    	
    	appointment = aC.getAppointment(appointmentID);
		client = cC.getClientByID(appointment.clientId);
		
    	clientInfo.setText(client.lastName + " " + client.firstName + "\n" + 
    			client.street + " " + client.houseNumber + "\n" + 
    			client.zip + " " + client.city);
    	notes.setText(getResources().getString(R.string.clientdetail_text_notes)+ ":\n" + appointment.note);
    	timeDate.setText(pad(appointment.from.get(Calendar.DAY_OF_MONTH)) + "." + 
    			pad(appointment.from.get(Calendar.MONTH)+1) + " " +
    			appointment.from.get(Calendar.YEAR));
    	timeFrom.setText( getResources().getString(R.string.add_new_appointment_button_timefrom) + ": " + pad(appointment.from.get(GregorianCalendar.HOUR_OF_DAY)) + ":"  + pad(appointment.from.get(GregorianCalendar.MINUTE)));
    	timeTo.setText(getResources().getString(R.string.add_new_appointment_button_timeto) + ": " + pad(appointment.to.get(GregorianCalendar.HOUR_OF_DAY)) + ":"  + pad(appointment.to.get(GregorianCalendar.MINUTE)));
    	timeDuration.setText(getResources().getString(R.string.add_new_appointment_button_duration) + ": "  + String.valueOf(appointment.duration) + "min");
    	completed.setChecked(appointment.completed);
    }
    
    /** Start Client Detail Activity
     * 
     * Used to start the detail view of the client.
     * 
     * @param id The id of the client
     */
    private void startClientDetailActivityIntent(String id) {
		Intent intent = new Intent(this, ClientDetailActivity.class); 
		intent.putExtra("id", id);
		startActivity(intent);
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.appointmentdateiloptionmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.appointment_detail_optionmenu_updateAppointment:      	
        	Intent intent = new Intent(this, UpdateAppointmentActivity.class);  
    		intent.putExtra("appointmentID", appointment.id);
    		startActivity(intent);
            return true;
        
        case R.id.appointment_detail_optionmenu_deleteAppointment:
        	onCreateDialog(DIALOG_YES_NO_MESSAGE_DELETEAPPOINTMENT);
        	return true;
        	
        	
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    protected Dialog onCreateDialog(int id)
	{

		switch (id)
		{
		case DIALOG_YES_NO_MESSAGE_DELETEAPPOINTMENT:
			return new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.dialog_text_delete))
			.setPositiveButton(getResources().getString(R.string.dialog_text_yes), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					
					if(!aC.deleteAppointment(appointment.id, appointment.rev)) {
						Log.e(TAG, "FAIL: Delete Appointment");
					}
					finish();
					Log.i(TAG, "Yes clicked.");
				}
			})
			.setNegativeButton(getResources().getString(R.string.dialog_text_no), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					Log.i(TAG, "No clicked.");
				}
			})
			.show();
		}
		return null;
	} 
    
    /** Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front of the string representation.
	 * 
	 * @param c The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
    private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
    
    @Override
	protected void onResume() {
		super.onResume();

		loadContent();
	}
}
