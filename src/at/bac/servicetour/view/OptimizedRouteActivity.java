/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

public class OptimizedRouteActivity extends Activity {

	//Layouts
	private LinearLayout linearMain;
	private ScrollView scroll;
	private LinearLayout linearIn;

	//Content vies
	private TextView headline1;
	private TextView headline2;
	private LinearLayout.LayoutParams layoutParams;

	//the requested Date
	private int day;
	private int month;
	private int year;
	private String[] orderedAppointmentsArray;
	private String orderedAppointmentsString;
	private String lastOrderedAppointmentsString;
	
	//DB Controller
	private AppointmentController aC;
	private ClientController cC;

	//Appointments of the requested date
	private ArrayList<AppointmentDomain> appointments;

	// Activity elements
	private GregorianCalendar dateToShow;
	private String[] weekDays;

	private boolean firstStart = true;
	private boolean toogle4CheckBox = true;
	
	private int actualCheckboxID;
	
	static final int SUBACTIVITY_CALCULATE = 1;
	private static final int DIALOG_YES_NO_MESSAGE_CALCULATEROUTE = 10;
	private static final int DIALOG_YES_NO_MESSAGE_UNCHECKED = 11;
	private static final String TAG = OptimizedRouteActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle map = getIntent().getExtras();
		
		day = (Integer) map.get("day");
		month = (Integer) map.get("month");
		year = (Integer) map.get("year");
		
		orderedAppointmentsString = map.getString("orderedAppointments");
		
		boolean showContent = true;
		if(orderedAppointmentsString.startsWith("ERROR:")) {
			showContent = false;
			
			new AlertDialog.Builder(this)
            .setMessage(orderedAppointmentsString)
            .setTitle("Error")
            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
 
                public void onClick(DialogInterface arg0, int arg1) {
                    finish();        
                }
            })
            .show();

		} else if(orderedAppointmentsString.equals("")) {
			showContent = false;
			
			new AlertDialog.Builder(this)
            .setMessage(getResources().getString(R.string.optimizedroute_error_nowayfound))
            .setTitle(getResources().getString(R.string.optimizedroute_error_nowayfound_title))
            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
 
                public void onClick(DialogInterface arg0, int arg1) {
                    finish();        
                }
            })
            .show();
		}
		
		aC = new AppointmentController();
		cC = new ClientController();
		
//		Log.d(TAG, "Service is running: " + isMyServiceRunning());
		
		if(showContent) buildView();
	}

	/** Is my service running
	 * 
	 * Returns true if the calculating service is running, otherwise false.
	 * 
	 * @return true if the service is running, otherwise false.
	 */
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("at.bac.servicetour.model.TsptwService".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	/** Build View
	 * 
	 * This method fills the activity with the required information.
	 * 
	 */
	private void buildView() {

		orderedAppointmentsArray = orderedAppointmentsString.split(" ");
		lastOrderedAppointmentsString = orderedAppointmentsString;
		
		appointments = aC.getAllAppointmentsByDateCompleted(new GregorianCalendar(this.year, this.month, this.day));
		
		AppointmentDomain tmp;
		for(int j = orderedAppointmentsArray.length-1; j >= 0 ; j--) {
			
			tmp = aC.getAppointment(orderedAppointmentsArray[j]);

			if(tmp != null && !tmp.completed) { // if a appointment was deleted
				Log.d(TAG, "in if ID: " + tmp.id);
				appointments.add(tmp);
			}
		}
		
		// if all appointments are deleted
		if(appointments.size() == 0) {
			finish();
		}
		
		ClientDomain c;
		LinearLayout contentLinearLayout;
		CheckBox cBox;
		TextView name;
		ImageButton imgB;
		
		if (appointments.size() > 0) {
			
			this.dateToShow = appointments.get(0).from;
			weekDays = getResources().getStringArray(R.array.weekdays_german);
			//Layout
			linearMain = new LinearLayout(this);
			linearMain.setOrientation(LinearLayout.VERTICAL);
			scroll = new ScrollView(this);
			linearIn = new LinearLayout(this);
			linearIn.setOrientation(LinearLayout.VERTICAL);
			scroll.addView(linearIn, new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			linearMain.addView(scroll, new ScrollView.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			//Headline1
			headline1 = new TextView(this);
			headline1.setTextSize(30);
			headline1.setText(weekDays[dateToShow
					.get(GregorianCalendar.DAY_OF_WEEK) - 1]);
			headline1.setGravity(Gravity.CENTER);
			headline1.setTextColor(getResources().getColor(
					R.color.servicetour_blue));
			headline1.setShadowLayer(1.5f, 1, 1,
					getResources().getColor(R.color.white));
			linearIn.addView(headline1);
			
			//Headline2
			headline2 = new TextView(this);
			headline2.setTextSize(20);
			headline2.setText(dateToShow.get(GregorianCalendar.DAY_OF_MONTH)
					+ "." + (dateToShow.get(GregorianCalendar.MONTH) + 1) + " "
					+ dateToShow.get(GregorianCalendar.YEAR));
			headline2.setGravity(Gravity.CENTER);
			headline2.setTextColor(getResources().getColor(R.color.orange));
			linearIn.addView(headline2);
	
			layoutParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			c = null;
		}
		for (int i = 0; i < this.appointments.size(); i++) {
			
			if(appointments.get(i) != null) {
			
				//Listentry
				contentLinearLayout = new LinearLayout(this);
				contentLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
				//			contentLinearLayout.setBaselineAligned(true);
				linearIn.addView(contentLinearLayout, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				Log.e(TAG, "Color: " + R.color.servicetour_blue);
				if(appointments.get(i).isPrivate) contentLinearLayout.setBackgroundColor(Color.GRAY);
	
				//Checkbox
				cBox = new CheckBox(this);
				cBox.setHeight(50);
				cBox.setId(i);
				if(appointments.get(i).completed) cBox.setChecked(true);
				cBox.setOnCheckedChangeListener(new OnCheckedChangeListener()
				{
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
					{
						if (isChecked)
						{
	//						Log.d(TAG, "isCheckt withID: " + buttonView.getId());
							if(aC.updateAppointment(new AppointmentDomain(appointments.get(buttonView.getId()).id, 
									appointments.get(buttonView.getId()).rev, 
									appointments.get(buttonView.getId()).from,
									appointments.get(buttonView.getId()).to, 
									appointments.get(buttonView.getId()).duration,
									appointments.get(buttonView.getId()).clientId,
									appointments.get(buttonView.getId()).isPrivate,
									appointments.get(buttonView.getId()).note,
									true))) {
								
								buildView();
								
							} else {
								Log.e(TAG, "FAIL: update Appointment");
							}
						} else {
							CheckBox c = (CheckBox) findViewById(buttonView.getId());
							c.setChecked(true);

							actualCheckboxID = buttonView.getId();
							if(toogle4CheckBox) {
								onCreateDialog(DIALOG_YES_NO_MESSAGE_UNCHECKED);
							} else {
								toogle4CheckBox = true;
							}
							
						}
	
					}
				});
				
				layoutParams = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutParams.setMargins(10, 20, 10, 0);
				layoutParams.weight = 1;
				contentLinearLayout.addView(cBox, layoutParams);
	
				//Name, duration, address
				c = cC.getClientByID(appointments.get(i).clientId);
				name = new TextView(this);
				//			name.setBackgroundColor(Color.WHITE);
				name.setClickable(true);
				name.setId(i);
				name.setWidth(280);
				name.setHeight(77);
				name.setText(c.lastName + " " + c.firstName + "\n" + 
						pad(appointments.get(i).from.get(Calendar.HOUR_OF_DAY)) + ":" +
						pad(appointments.get(i).from.get(Calendar.MINUTE)) + " - " + pad(appointments.get(i).to.get(Calendar.HOUR_OF_DAY)) + ":" +
						pad(appointments.get(i).to.get(Calendar.MINUTE)) + " (" + appointments.get(i).duration + "min)" + "\n" +
						c.street + " " + c.houseNumber + ", " + c.zip + " "  + c.city);	
				name.setOnClickListener(new View.OnClickListener() {
	
					public void onClick(View v) {
						if(v instanceof TextView) {
							Log.d(TAG, "ID of Textview:" + v.getId());
							startAppointmentDetailActivityIntent(appointments.get(v.getId()).id);
						} else {
							Log.d(TAG, "no Textview");
						}
					}
				});	
				contentLinearLayout.addView(name);
	
				//ImageButton
				imgB = new ImageButton(this);
				imgB.setImageResource(R.drawable.ic_navi);
				imgB.setBackgroundColor(0);
				imgB.setId(i);
				imgB.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						
						//Navigation
						ClientDomain c = cC.getClientByID(appointments.get(v.getId()).clientId);
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse("google.navigation:q=" +
										c.street + "+" + c.houseNumber + "+" +
										c.zip + "+" + c.city));
						startActivity(intent);
					}
				});
				layoutParams = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutParams.weight = 1;
				layoutParams.height = 100;
				layoutParams.width = 90;
				layoutParams.gravity = Gravity.LEFT;
				layoutParams.setMargins(20, 0, 0, 0);
				contentLinearLayout.addView(imgB, layoutParams);
	
				//Separatorline
				View s = new View(this);
				s.setBackgroundColor(Color.WHITE);	
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				params.setMargins(0, 20, 0, 20);
				params.height = 1;
				linearIn.addView(s, params);
			}
	
	
			//sets the created view
			setContentView(linearMain);
		}
	}

	/** Start Appointment Detail Activity
	 * 
	 * Starts the detail activity of an appointment
	 * 
	 * @param id The id of the requested appointment
	 */
	private void startAppointmentDetailActivityIntent(String id) {
		Intent intent = new Intent(this, AppointmentDetailActivity.class); 
		intent.putExtra("id", id);
		startActivity(intent);
	}


	@Override
	protected void onResume() {
		if(this.firstStart) {
			this.firstStart = false;
		} else {
			onCreateDialog(DIALOG_YES_NO_MESSAGE_CALCULATEROUTE);
			buildView();
		}
		super.onResume();
	}

	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case DIALOG_YES_NO_MESSAGE_CALCULATEROUTE:
			return new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.dialog_text_calculateroute))
			.setPositiveButton(getResources().getString(R.string.dialog_text_yes), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					startCalculatingRoute();
					Log.i(TAG, "Yes clicked.");
				}
			})
			.setNegativeButton(getResources().getString(R.string.dialog_text_no), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					Log.i(TAG, "No clicked.");
				}
			})
			.show();
		
		case DIALOG_YES_NO_MESSAGE_UNCHECKED:
			return new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.optimized_route_alertbox_newcalculation))
			.setMessage(getResources().getString(R.string.optimized_route_alertbox_newcalculation_text))
			.setPositiveButton(getResources().getString(R.string.dialog_text_yes), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					Log.d(TAG, "actualCheckboxID: " + actualCheckboxID);
					if(aC.updateAppointment(new AppointmentDomain(appointments.get(actualCheckboxID).id, 
							appointments.get(actualCheckboxID).rev,
							appointments.get(actualCheckboxID).from,
							appointments.get(actualCheckboxID).to, 
							appointments.get(actualCheckboxID).duration,
							appointments.get(actualCheckboxID).clientId,
							appointments.get(actualCheckboxID).isPrivate,
							appointments.get(actualCheckboxID).note,
							false))) {
												
						startCalculatingRoute();
						
					} else {
						Log.e(TAG, "FAIL: update Appointment");
					}
					
					Log.i(TAG, "Yes clicked.");
				}
			})
			.setNegativeButton(getResources().getString(R.string.dialog_text_no), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					
					CheckBox c = (CheckBox) findViewById(actualCheckboxID);
					toogle4CheckBox = false;
					c.setChecked(false);
					
					Log.i(TAG, "No clicked.");
				}
			})
			.show();	
		}
		return null;
	} 

	/** Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front of the string representation.
	 * 
	 * @param c The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
	private String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.appointmentsoptionmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		Intent intent;
		switch (item.getItemId()) {
		case R.id.appointments_optionmenu_newAppointment:
			intent = new Intent(this, AddNewAppointmentActivity.class);
			intent.putExtra("day", this.dateToShow.get(GregorianCalendar.DAY_OF_MONTH));
			intent.putExtra("month", this.dateToShow.get(GregorianCalendar.MONTH));
			intent.putExtra("year", this.dateToShow.get(GregorianCalendar.YEAR));
			intent.putExtra("setClient", false);
			startActivity(intent);
			return true;
		case R.id.appointments_optionmenu_calroute:

			if (!appointments.isEmpty()) {
				
				startCalculatingRoute();
			} else {
				Toast.makeText(this,
						R.string.appointments_toast_noappointments,
						Toast.LENGTH_LONG).show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
		case Activity.RESULT_OK:
//			orderedAppointmentsArray = data.getExtras().getString("orderedAppointments").split(" ");
			boolean showContent = true;
			if(orderedAppointmentsString.startsWith("ERROR:")) {
				showContent = false;
				new AlertDialog.Builder(this)
	            .setMessage(orderedAppointmentsString)
	            .setTitle("Error")
	            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
 
                public void onClick(DialogInterface arg0, int arg1) {
                          
                }
            })
	            .show();
				orderedAppointmentsString = lastOrderedAppointmentsString;
			} else if(orderedAppointmentsString.equals("")) {
				showContent = false;
				new AlertDialog.Builder(this)
	            .setMessage(getResources().getString(R.string.optimizedroute_error_nowayfound))
	            .setTitle(getResources().getString(R.string.optimizedroute_error_nowayfound_title))
	            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
 
                public void onClick(DialogInterface arg0, int arg1) {
                        
                }
            })
	            .show();
				orderedAppointmentsString = lastOrderedAppointmentsString;
			} else {
				orderedAppointmentsString = data.getExtras().getString("orderedAppointments");
			}
			
			if(showContent) buildView();
			break;
		case Activity.RESULT_CANCELED:
			Log.e(TAG, "FAIL: wrong callback from calculate Route or back");
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/** Start Calculating Route
	 * 
	 * Starts the calculation of an route as a sub-activity.
	 * 
	 */
	private void startCalculatingRoute() {
		this.firstStart = true;
		
		Intent intent = new Intent(this, CalculateRouteActivity.class);
		intent.putExtra("day",
				this.dateToShow.get(GregorianCalendar.DAY_OF_MONTH));
		intent.putExtra("month",
				this.dateToShow.get(GregorianCalendar.MONTH));
		intent.putExtra("year",
				this.dateToShow.get(GregorianCalendar.YEAR));
		intent.putExtra(
				"textToShow",
				getResources().getString(
						R.string.calculate_route_textview_position));
		intent.putExtra("SUBACTIVITY_CALCULATE", true);
		intent.putExtra("startedAsSubactivity", true);
		startActivityForResult(intent, SUBACTIVITY_CALCULATE);
	}
}
