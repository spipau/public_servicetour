/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.controller.IAppointmentController;
import at.bac.servicetour.controller.IClientController;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/** Update Appointment Activity
 * 
 * With this activity the user can update the information in the database
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class UpdateAppointmentActivity extends Activity {

	// Activity elements
	private Button dateButton;
	private Button timetoButton;
	private Button timefromButton;
	private Button saveButton;
	private Button addClientButton;

	private TextView clientTextView;

	private EditText durationEditText;
	private EditText noteEditText;

	private CheckBox isPrivateCheckBox;
	
	private int mYear;
	private int mMonth;
	private int mDay;
	private int mHourFrom;
	private int mMinuteFrom;
	private int mHourTo;
	private int mMinuteTo;

	static final int DATE_DIALOG_ID = 0;
	static final int TIME_DIALOG_FROM_ID = 1;
	static final int TIME_DIALOG_TO_ID = 2;
	static final int DIALOG_DURATION_ID = 3;

	static final int SUBACTIVITY_PICKCLIENT = 0;

	// DB helper
	private IAppointmentController aC = new AppointmentController();
	private IClientController cC = new ClientController();

	private ClientDomain client;
	private AppointmentDomain appointment;

	private static final String TAG = UpdateAppointmentActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointmentupdateappointment);

		dateButton = (Button) findViewById(R.id.update_appointment_button_date);
		timefromButton = (Button) findViewById(R.id.update_appointment_button_from);
		timetoButton = (Button) findViewById(R.id.update_appointment_button_to);
		saveButton = (Button) findViewById(R.id.update_appointment_button_save);
		addClientButton = (Button) findViewById(R.id.update_appointment_button_addclient);

		clientTextView = (TextView) findViewById(R.id.update_appointment_textview_client);

		durationEditText = (EditText) findViewById(R.id.update_appointment_edittext_duration);
		noteEditText = (EditText) findViewById(R.id.update_appointment_editText_note);

		isPrivateCheckBox = (CheckBox) findViewById(R.id.update_appointment_checkBox_private);
		
		Bundle map = getIntent().getExtras();
		appointment = aC.getAppointment(map.getString("appointmentID"));
		client = cC.getClientByID(appointment.clientId);

		mYear = appointment.from.get(Calendar.YEAR);
		mMonth = appointment.from.get(Calendar.MONTH);
		mDay = appointment.from.get(Calendar.DAY_OF_MONTH);


		mHourFrom = appointment.from.get(Calendar.HOUR_OF_DAY);
		mHourTo = appointment.to.get(Calendar.HOUR_OF_DAY);
		mMinuteFrom = appointment.from.get(Calendar.MINUTE);
		mMinuteTo = appointment.to.get(Calendar.MINUTE);

		durationEditText.setText(String.valueOf(appointment.duration));
		noteEditText.setText(appointment.note);
		
		isPrivateCheckBox.setChecked(appointment.isPrivate);
		

		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				updateAppointment();
			}

		});

		addClientButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startSubActivity(SUBACTIVITY_PICKCLIENT);
			}
		});

		dateButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		timefromButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_FROM_ID);
			}
		});

		timetoButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_TO_ID);
			}
		});

		updateDisplay();
	}

	/** Start Sub Activity
	 * 
	 * Starts the PickClientActivity as a sub-activity, so the user can pick a client
	 * 
	 * @param id The id of the sub-activity
	 */
	private void startSubActivity(int id) {
		switch(id) {
		case SUBACTIVITY_PICKCLIENT:
			Intent intent = new Intent(this, PickClientActivity.class);
			intent.putExtra("SUBACTIVITY_PICKCLIENT", true);

			startActivityForResult(intent, SUBACTIVITY_PICKCLIENT);
			break;
		}
	}

	/** Update Display
	 * 
	 * Fills the display with the required information form the database
	 * 
	 */
	private void updateDisplay() {

		String[] weekDays = getResources().getStringArray(R.array.weekdays_german);

		GregorianCalendar cal = new GregorianCalendar();

		cal.set( Calendar.DATE, mDay );
		cal.set( Calendar.MONTH, mMonth );
		cal.set( Calendar.YEAR, mYear );

		dateButton.setText( 
				new StringBuilder()
				// Month is 0 based so add 1
				.append(weekDays[cal.get(Calendar.DAY_OF_WEEK) -1]).append(" ")
				.append(mDay).append(".")
				.append(mMonth + 1).append(".")
				.append(mYear).append(" "));

		timefromButton.setText(
				new StringBuilder()
				.append(pad(mHourFrom)).append(":")
				.append(pad(mMinuteFrom)));

		timetoButton.setText(
				new StringBuilder()
				.append(pad(mHourTo)).append(":")
				.append(pad(mMinuteTo)));


		clientTextView.setText(client.lastName + " " + client.firstName);
	}

	// Date Picker Dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener =
		new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, 
				int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	// Time Picker Dialog From
	private TimePickerDialog.OnTimeSetListener mTimeFromSetListener =
		new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			mHourFrom = hourOfDay;
			mMinuteFrom = minute;
			mHourTo = (hourOfDay % 24) + 1;
			mMinuteTo = minute;
			updateDisplay();
		}
	};

	// Time Picker Dialog To
	private TimePickerDialog.OnTimeSetListener mTimeToSetListener =
		new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			if(mHourFrom <= hourOfDay) {				
				if(mHourFrom == hourOfDay) {				
					if (mMinuteFrom < minute) {				
						mHourTo = hourOfDay;
						mMinuteTo = minute;
						updateDisplay();
					} else {
						showErrorMessage(getResources().getString(R.string.add_new_appointment_text_wrongtotime));
					}
				} else {
					Log.d(TAG,"4");
					mHourTo = hourOfDay;
					mMinuteTo = minute;
					updateDisplay();
				}
			} else {
				showErrorMessage(getResources().getString(R.string.add_new_appointment_text_wrongtotime));
			}
		}
	};

	/** Show Error Message
	 * 
	 * Displays an error message as an alert box.
	 * 
	 * @param message The error message
	 */
	private void showErrorMessage(String message) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage(message);	 
		alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface arg0, int arg1) {   
			}
		});
		alertbox.show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this,
					mDateSetListener,
					mYear, mMonth, mDay);
		case TIME_DIALOG_FROM_ID:
			return new TimePickerDialog(this,
					mTimeFromSetListener, appointment.from.get(Calendar.HOUR_OF_DAY), appointment.from.get(Calendar.MINUTE), true); 
		case TIME_DIALOG_TO_ID:
			return new TimePickerDialog(this,
					mTimeToSetListener, appointment.to.get(Calendar.HOUR_OF_DAY), appointment.to.get(Calendar.MINUTE), true);
		}
		return null;
	}

	/** Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front of the string representation.
	 * 
	 * @param c The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	/** Update Appointment
	 * 
	 * Saves the changes to the database
	 * 
	 */
	private void updateAppointment() {

		appointment.from.set(mYear, mMonth, mDay, mHourFrom, mMinuteFrom, 0);
		appointment.to.set(mYear, mMonth, mDay, mHourTo, mMinuteTo, 0);
		appointment.isPrivate = this.isPrivateCheckBox.isChecked();
		appointment.clientId = this.client.id;
		appointment.duration = Integer.valueOf(durationEditText.getText().toString());
		appointment.note = noteEditText.getText().toString();
		
		if (appointment.duration <= ((mHourTo-mHourFrom)*60 + (mMinuteTo-mMinuteFrom))) {
			
			if (aC.updateAppointment(appointment)) {
				Toast.makeText(
						this,
						getResources()
						.getString(
								R.string.add_new_appointment_text_appointmentsaved),
								Toast.LENGTH_LONG).show();
				finish();
			} else {
				showErrorMessage(getResources().getString( 
						R.string.add_new_appointment_text_savefail));
			}
		} else {
			showErrorMessage(getResources().getString(R.string.add_new_appointment_text_tolongduration));
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
		case Activity.RESULT_OK:
			this.client = cC.getClientByID(data.getExtras().getString(PickClientActivity.CLIENT_ID));
			updateDisplay();
			break;
		case Activity.RESULT_CANCELED:
			Log.e(TAG, "FAIL: wrong callback from pickclient or back");
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
