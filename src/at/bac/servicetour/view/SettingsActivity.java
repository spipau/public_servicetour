/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.controller.IClientController;

/**
 * Settings Activity
 * 
 * Displays the settings activity where the user can set some settings
 * 
 * @author Alois Paul Spiesberger-H�ckner
 * 
 */
public class SettingsActivity extends Activity {

	private Button timetoButton;
	private Button aboutButton;

	private Button addData;
	private Button delData;

	private TextView worktime;

	private EditText usernameEditText;
	private EditText pwEditText;
	private EditText serverEditText;
	private EditText dbEditText;

	private int mHourTo;
	private int mMinuteTo;

	private static final int TIME_DIALOG_TO_ID = 2;
	public static final String PREFS_NAME = "settings_servicetour";

	private SharedPreferences settings;

	private IClientController cC = new ClientController(); // nur f�r testdaten

	private static final String TAG = SettingsActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		timetoButton = (Button) findViewById(R.id.settings_button_to);
		aboutButton = (Button) findViewById(R.id.settings_button_aboutservicetour);

		// addData = (Button) findViewById(R.id.testdaten);
		// delData = (Button) findViewById(R.id.deldata);

		worktime = (TextView) findViewById(R.id.settings_textview_worktime);

		usernameEditText = (EditText) findViewById(R.id.settings_edittext_username);
		pwEditText = (EditText) findViewById(R.id.settings_edittext_pw);
		serverEditText = (EditText) findViewById(R.id.settings_edittext_server);
		dbEditText = (EditText) findViewById(R.id.settings_edittext_db);

		// addData.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// new Testdaten();
		// }
		// });
		//
		// delData.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// ArrayList<ClientDomain> clients = cC.getAllClients();
		// for (ClientDomain c : clients) {
		// cC.deleteClientById(c.id, c.rev);
		// }
		// }
		// });

		timetoButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_TO_ID);
			}
		});

		aboutButton.setOnClickListener(new View.OnClickListener() {
			private Dialog dialog;

			public void onClick(View v) {
				dialog = new Dialog(SettingsActivity.this);
				dialog.setContentView(R.layout.dialog_aboutservicetour);
				dialog.setTitle("ServiceTour");
				dialog.setCancelable(true);
				// there are a lot of settings, for dialog, check them all out!

				// set up button
				Button button = (Button) dialog.findViewById(R.id.dialog_about_button_ok);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						closeDialog();
					}
				});
				// now that the dialog is set up, it's time to show it
				dialog.show();
			}

			private void closeDialog() {
				dialog.cancel();
			}
		});

		settings = getSharedPreferences(PREFS_NAME, 0);
		mHourTo = settings.getInt("mHourTo", 8);
		mMinuteTo = settings.getInt("mMinuteTo", 0);

		updateDisplay();

	}

	/**
	 * Update Display
	 * 
	 * Fills the display with the required information form the database
	 * 
	 */
	private void updateDisplay() {
		timetoButton.setText(pad(mHourTo) + ":" + pad(mMinuteTo));
		worktime.setText(getResources().getString(R.string.settings_textview_text_worktime));

		usernameEditText.setText(settings.getString("usernameSync", ""));
		pwEditText.setText(settings.getString("pwSync", ""));
		serverEditText.setText(settings.getString("serverSync", ""));
		dbEditText.setText(settings.getString("dvSync", ""));
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_TO_ID:
			return new TimePickerDialog(this, mTimeToSetListener, mHourTo, mMinuteTo, true);
		}
		return null;
	}

	// Time Picker Dialog To
	private TimePickerDialog.OnTimeSetListener mTimeToSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHourTo = hourOfDay;
			mMinuteTo = minute;
			updateDisplay();
		}
	};

	@Override
	protected void onStop() {
		super.onStop();

		// Save worktime
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("mHourTo", mHourTo);
		editor.putInt("mMinuteTo", mMinuteTo);

		editor.putString("usernameSync", usernameEditText.getText().toString());
		editor.putString("pwSync", pwEditText.getText().toString());
		editor.putString("serverSync", serverEditText.getText().toString());
		editor.putString("dvSync", dbEditText.getText().toString());

		editor.commit();
	}

	/**
	 * Pad
	 * 
	 * If a date or time is under the amount of 10, then a 0 is added in front
	 * of the string representation.
	 * 
	 * @param c
	 *            The value
	 * @return If the value is >= 10 the the value, else the 0 + value
	 */
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
}
