/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.ClientDomain;

/** Clients Activity
 * 
 * Displays a list of all clients in the database. 
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class ClientsActivity extends Activity {

	private static final String TAG = ClientsActivity.class.getSimpleName();

	// DB helper
	private ClientController cC = new ClientController();

	// Activity elements
	private HashMap<Long, String> clientHashMap;
	private ListView lv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.clients);

		lv = (ListView) findViewById(R.id.clients_list); // List
		lv.setTextFilterEnabled(true);

		buildList();

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (id != -1) {
					startClientDetailActivityIntent(clientHashMap.get(id));
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.clientsoptionmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.clients_optionmenu_newClient:

			Intent intent = new Intent(this, AddNewClientActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/** Build List
	 * 
	 * This method fills the list with the required information.
	 * 
	 */
	private void buildList() {

		ArrayList<ClientDomain> clients = cC.getAllClients(); // DB
		
		// Layout of the list
		final String[] columns = { "Zeile1", "Zeile2" };
		final String[] matrix = { "_id", "Zeile1", "Zeile2" };
		final int[] layouts = new int[] { android.R.id.text1, android.R.id.text2 };
		long key = 0;

		MatrixCursor cursor = new MatrixCursor(matrix);

		if (clients.isEmpty()) {
			key = -1;
			cursor.addRow(new Object[] { key, getResources().getString(R.string.clients_text_noClientsinDB), "" });
		} else {
			this.clientHashMap = new HashMap<Long, String>();
			for (ClientDomain c : clients) {
				// Log.d(TAG, "KEY: " + key);
				key = key + 1;
				this.clientHashMap.put(key, c.id);
				cursor.addRow(new Object[] { key, c.lastName + " " + c.firstName, c.street + " " + c.houseNumber + ", " + c.zip + " " + c.city });
			}
		}

		SimpleCursorAdapter data = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columns, layouts);

		lv.setAdapter(data);
	}

	/** Start Client Detail Activity
	 * 
	 * Starts the client detail activity of an chosen list entry.
	 * 
	 * @param id The id of the client
	 */
	private void startClientDetailActivityIntent(String id) {
		Intent intent = new Intent(this, ClientDetailActivity.class);
		intent.putExtra("id", id);
		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();

		buildList();
	}
}
