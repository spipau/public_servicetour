/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.AppointmentController;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/** Show Appointments of a Client
 * 
 * Displays the future or the past appointments of a client.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class ClientShowAppointmentsActivity extends Activity {

	private static final String TAG = ClientShowAppointmentsActivity.class.getSimpleName();

	private HashMap<Long, String> appointmentHashMap;

	// DB helper
	private AppointmentController aC = new AppointmentController();
	private ClientController cC = new ClientController();

	// Activity elements
	private TextView nameTextView;
	private TextView desTextView;

	private ListView lv;

	private boolean showFuture;
	private String clientId;
	private ClientDomain client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.clientshowappointments);

		Bundle map = getIntent().getExtras();
		this.showFuture = (Boolean) map.get("showFuture");
		this.clientId = (String) map.getString("id");
		this.client = cC.getClientByID(this.clientId);

		nameTextView = (TextView) findViewById(R.id.client_show_appointments_textview_name);
		desTextView = (TextView) findViewById(R.id.client_show_appointments_textview_des);

		lv = (ListView) findViewById(R.id.client_show_appointments_listview);
		lv.setTextFilterEnabled(true);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (id != -1) {
					startAppointmentDetailActivityIntent(appointmentHashMap.get(id));
				}
			}
		});

		buildList();
	}

	/** Build List
	 * 
	 * This method fills the list with the required information.
	 * 
	 */
	private void buildList() {

		String[] weekDays = getResources().getStringArray(R.array.weekdays_german);
		this.nameTextView.setText(this.client.lastName + " " + this.client.firstName);
		this.desTextView.setText(getResources().getString(R.string.client_show_appointments_textview_des));

		ArrayList<AppointmentDomain> appointments;
		if (showFuture) {
			appointments = aC.getAllFutureAppointmentsByClient(clientId);
		} else {
			appointments = aC.getAllPastAppointmentsByClient(clientId);
		}

		final String[] columns = { "Zeile1", "Zeile2" };
		final String[] matrix = { "_id", "Zeile1", "Zeile2" };
		final int[] layouts = new int[] { android.R.id.text1, android.R.id.text2 };
		long key = 0;

		MatrixCursor cursor = new MatrixCursor(matrix);

		if (appointments.isEmpty()) {
			Log.d(TAG, "appointments isEmty true!");
			cursor.addRow(new Object[] { -1, getResources().getString(R.string.clients_text_noClientsinDB), "" });
		} else {
			this.appointmentHashMap = new HashMap<Long, String>();
			for (AppointmentDomain a : appointments) {

				key = key + 1;
				this.appointmentHashMap.put(key, a.id);

				cursor.addRow(new Object[] { key, weekDays[a.from.get(GregorianCalendar.DAY_OF_WEEK) - 1] + " " + a.from.get(Calendar.DAY_OF_MONTH) + "." + a.from.get(Calendar.MONTH) + " " + a.from.get(Calendar.YEAR), a.from.get(GregorianCalendar.HOUR_OF_DAY) + ":" + a.from.get(GregorianCalendar.MINUTE) + " - " + a.to.get(GregorianCalendar.HOUR_OF_DAY) + ":" + a.to.get(GregorianCalendar.MINUTE) + " (" + a.duration + "min)" });
			}
		}

		SimpleCursorAdapter data = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columns, layouts);

		lv.setAdapter(data);

	}

	/** Start Appointment Detail Activity
	 * 
	 * Starts the appointment detail activity of an chosen list entry.
	 * 
	 * @param id The id of the appointment
	 */
	private void startAppointmentDetailActivityIntent(String id) {
		Intent intent = new Intent(this, AppointmentDetailActivity.class);
		intent.putExtra("id", id);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.clientshowappointmentsoptionmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		Intent intent;
		switch (item.getItemId()) {
		case R.id.client_show_appointments_optionmenu_text_addnewappointment:
			GregorianCalendar cal = new GregorianCalendar();
			intent = new Intent(this, AddNewAppointmentActivity.class);
			intent.putExtra("day", cal.get(GregorianCalendar.DAY_OF_MONTH));
			intent.putExtra("month", cal.get(GregorianCalendar.MONTH));
			intent.putExtra("year", cal.get(GregorianCalendar.YEAR));
			intent.putExtra("setClient", true);
			intent.putExtra("clientID", this.clientId);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		buildList();
	}
}
