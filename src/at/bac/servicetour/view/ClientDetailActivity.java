/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.ClientDomain;

/** Client Detail Activity
 * 
 * Displays all client details. 
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class ClientDetailActivity extends Activity implements OnClickListener {

	private static final String TAG = ClientDetailActivity.class.getSimpleName();

	// Activity elements
	private Button buttonAppointments;
	private Button buttonHistory;
	private Button showAddress;
	
	private ClientController cC = new ClientController();

	private String clientID;
	private ClientDomain client;

	private static final int DIALOG_YES_NO_MESSAGE_DELETECLIENT = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.clientdetail);

		Bundle map = getIntent().getExtras();

		clientID = (String) map.get("id");
		
		loadContent();


		buttonAppointments = (Button) findViewById(R.id.clientdetail_button_appointments);
		buttonHistory = (Button) findViewById(R.id.clientdetail_button_history);


		buttonAppointments.setOnClickListener(this);
		buttonHistory.setOnClickListener(this);
		
		showAddress = (Button) findViewById(R.id.client_detail_button_showaddress);
        showAddress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
				Uri.parse("geo:0,0?q=" + client.street.replace(" ", "+") + "+" + client.houseNumber.replace(" ", "+") + "+" + client.zip.toString().replace(" ", "+") + "+" + client.city.replace(" ", "+")));
				startActivity(intent);

			}

		});

	}

	@Override
	public void onClick(View v) {

		Intent intent;
		switch(v.getId()) {
		case R.id.clientdetail_button_appointments: 
			intent = new Intent(this, ClientShowAppointmentsActivity.class);
			intent.putExtra("showFuture", true);
			intent.putExtra("id", clientID);
			startActivity(intent);
			break;
		case R.id.clientdetail_button_history: 
			intent = new Intent(this, ClientShowAppointmentsActivity.class);
			intent.putExtra("showFuture", false);
			intent.putExtra("id", clientID);
			startActivity(intent);
			break;
		case R.id.clientdetail_button_phone:

			try {
				intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:"+client.phone));
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Log.e(TAG, "Call failed" , e);
				Toast.makeText(this,
						R.string.clientdetail_text_callfail,
						Toast.LENGTH_LONG).show();
			}

			break;

		case R.id.clientdetail_button_mobil:

			try {
				intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:"+client.mobil));
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Log.e(TAG, "Call failed" , e);
				Toast.makeText(this,
						R.string.clientdetail_text_callfail,
						Toast.LENGTH_LONG).show();
			}

			break;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.clientdetailoptionmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.clientdetailactivity_optionmenu_updateclient:

			Intent intent = new Intent(this, UpdateClientActivity.class);  
			intent.putExtra("id", client.id);
			startActivity(intent);
			return true;

		case R.id.clientdetailactivity_optionmenu_deleteclient:
			onCreateDialog(DIALOG_YES_NO_MESSAGE_DELETECLIENT);
			return true;


		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		loadContent();
	}

	/** Load Content
	 * 
	 * Loads the content with the information of the database
	 */
	private void loadContent() {

		client = cC.getClientByID(clientID);
		
		TextView t = (TextView)findViewById(R.id.clientdetail_textview_info); 
		String zip;
		if(client.zip == 0) {
			zip = "";
		} else {
			zip = "" + client.zip;
		}

		t.setText(client.lastName + " " + client.firstName + "\n\n" + client.street + " " + client.houseNumber
				+ "\n" + zip + " " + client.city);

		TextView tphone = (TextView)findViewById(R.id.clientdetail_textview_phone);

		Button buttonCallPhone 	= 	(Button) findViewById(R.id.clientdetail_button_phone);
		buttonCallPhone.setOnClickListener(this);

		Button buttonCallMobil 	= 	(Button) findViewById(R.id.clientdetail_button_mobil);
		buttonCallMobil.setOnClickListener(this);

		if(!client.phone.equals("")) {

			tphone.setText(client.phone);
		} else {
			tphone.setText(getResources().getString(R.string.clientdetail_text_nonumber));
			buttonCallPhone.setClickable(false);
		}

		TextView tmobil = (TextView)findViewById(R.id.clientdetail_textview_mobil);
		if(!client.mobil.equals("")) {
			tmobil.setText(client.mobil);
		} else {
			tmobil.setText(getResources().getString(R.string.clientdetail_text_nonumber));
			buttonCallMobil.setClickable(false);
		}

		TextView note = (TextView) findViewById(R.id.clientdetail_textview_note);
		note.setText(getResources().getString(R.string.clientdetail_text_notes)+ ":\n" + client.note);
	}

	@Override
	protected Dialog onCreateDialog(int id)
	{

		switch (id)
		{
		case DIALOG_YES_NO_MESSAGE_DELETECLIENT:
			return new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.dialog_text_delete))
			.setPositiveButton(getResources().getString(R.string.dialog_text_yes), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					
					if(!cC.deleteClientById(client.id, client.rev)) {
						Log.e(TAG, "FAIL: Delete Client");
					}
					finish();
					Log.i(TAG, "Yes clicked.");
				}
			})
			.setNegativeButton(getResources().getString(R.string.dialog_text_no), new
					DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int
						whichButton) {
					Log.i(TAG, "No clicked.");
				}
			})
			.show();
		}
		return null;
	} 
}
