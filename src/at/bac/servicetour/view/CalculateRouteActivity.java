/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import at.bac.servicetour.R;
import at.bac.servicetour.model.TsptwService;

/**
 * Calculate Route Activity
 * 
 * This Activity displays only a "busy screen" and inform the User what's
 * happening. In the background the GPS position is determined and is used for
 * the algorithm.
 * 
 * 1. Find the GPS Position 2. Use the new Information for the calculation.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 * 
 */
public class CalculateRouteActivity extends Activity {

	// Helper for GPS-Position
	private LocationListener mlocListener;
	private LocationManager mlocManager;

	private Bundle map;

	public double gpsPosLatitud; // GPS latitude
	public double gpsPosLongitud; // GPS longitude

	// Activity elements
	public String textToShow;

	private TextView infoTextView;

	private static final String TAG = CalculateRouteActivity.class.getSimpleName();

	private boolean firstStart = true;

	private boolean startedAsSubactivity;
	private boolean calculationRunning = false;

	// Binder for Service
	private TsptwService.TsptwBinder mTsptwBinder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calculateroute);

		// Deactivates the display timeout
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// GPS Position
		mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		mlocListener = new MyLocationListener(this);

		mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
		// GPS Position END

		map = getIntent().getExtras();
		textToShow = map.getString("textToShow");
		startedAsSubactivity = map.getBoolean("startedAsSubactivity");

		infoTextView = (TextView) findViewById(R.id.busy_textview_infotext);
		loadContent(textToShow);

		// startActivity(""); //wieder l�schen
		// calculationRunning = true;
		// optimizeRoute(); // F�r Testzwecke
	}

	public void loadContent(String textToShow) {
		infoTextView.setText(textToShow);
	}

	// Handler for the started service. Receives the message from the service.
	private final Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			final Bundle bundle = msg.getData();
			startNewActivity(bundle.getString("optimizedSucceeded"));

			super.handleMessage(msg);
		}
	};

	// ServiceConnection to Service - onServiceConnected is called when Service
	// is connected and
	// then the route will be optimized.
	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder binder) {
			mTsptwBinder = (TsptwService.TsptwBinder) binder;
			mTsptwBinder.setActivityCallbackHandler(messageHandler);
			// mTsptwBinder.setDataForTsptwCalculation(map.getInt("day"),
			// map.getInt("month"), map.getInt("year"), "48.207581005990505" +
			// "+" + "16.334159038960934");
			mTsptwBinder.setDataForTsptwCalculation(map.getInt("day"), map.getInt("month"), map.getInt("year"), gpsPosLatitud + "+" + gpsPosLongitud);
			mTsptwBinder.optimizeRoute();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			Log.i(TAG, "*********************\n\n\n\n\n\n\n\nSERVICE DISCONNECTED\n\n\n\n\n\n\n\n*********************");
		}
	};

	/**
	 * Start new activity
	 * 
	 * Starts a new activity. If the algorithm is called the first time, then
	 * the activity OptimizedRouteActivity is called. Otherwise the activity
	 * ends as a sub-activity. In both ways the service will be unbind.
	 * 
	 * @param orderedAppointments
	 *            The result of the algorithm.
	 */
	public void startNewActivity(String orderedAppointments) {

		Log.i(TAG, "New Activity start");
		if (isMyServiceRunning()) {
			unbindService(mServiceConnection);
			stopService(new Intent(this, TsptwService.class));
		}
		if (startedAsSubactivity) {
			final Intent intent = new Intent();
			intent.putExtra("orderedAppointments", orderedAppointments);
			setResult(Activity.RESULT_OK, intent);
			finish();
		} else {
			Intent intent = new Intent(this, OptimizedRouteActivity.class);
			intent.putExtra("day", map.getInt("day"));
			intent.putExtra("month", map.getInt("month"));
			intent.putExtra("year", map.getInt("year"));
			intent.putExtra("orderedAppointments", orderedAppointments);
			startActivity(intent);
		}
	}

	/**
	 * Optimize Route
	 * 
	 * Starts a new service to calculate the optimal route.
	 */
	public void optimizeRoute() {
		loadContent(getResources().getString(R.string.calculate_route_textview_optimizeroute));

		// starts the Service and bind it
		final Intent tsptwServiceIntent = new Intent(this, TsptwService.class);
		bindService(tsptwServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		// GPS unbind
		mlocManager.removeUpdates(mlocListener);

		if (isMyServiceRunning()) {
			unbindService(mServiceConnection);
			stopService(new Intent(this, TsptwService.class));
		}
		super.onStop();
	}

	@Override
	protected void onResume() {

		// final Intent tsptwServiceIntent = new Intent(this,
		// TsptwService.class);
		// bindService(tsptwServiceIntent, mServiceConnection,
		// Context.BIND_AUTO_CREATE);

		if (firstStart) {
			firstStart = false;
		} else {
			finish();
		}
		super.onResume();
	}

	/**
	 * Is my service running
	 * 
	 * Returns true if the calculating service is running, otherwise false.
	 * 
	 * @return true if the service is running, otherwise false.
	 */
	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("at.bac.servicetour.model.TsptwService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * My Location Listener
	 * 
	 * This inner class implements a LocationListener and can be used to find
	 * the actual GPS-position.
	 * 
	 * @author Alois Paul Spiesberger-H�ckner
	 * 
	 */
	public class MyLocationListener implements LocationListener

	{
		private final String TAG = MyLocationListener.class.getSimpleName();
		CalculateRouteActivity main;

		public MyLocationListener(CalculateRouteActivity main) {
			this.main = main;
		}

		@Override
		public void onLocationChanged(Location loc) {

			loc.getLatitude();
			loc.getLongitude();
			
//			String text = "My current location is: " +
//
//			"Latitud = " + loc.getLatitude() + "Longitud = " + loc.getLongitude();
//
//			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
//			Log.i(TAG, text);

			main.mlocManager.removeUpdates(this);
			main.gpsPosLatitud = loc.getLatitude();
			main.gpsPosLongitud = loc.getLongitude();

			main.calculationRunning = true;

			main.optimizeRoute();
		}

		@Override
		public void onProviderDisabled(String provider) {

			Toast.makeText(getApplicationContext(), getResources().getString(R.string.calculateroute_gpsdisabled), Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);

		}

		@Override
		public void onProviderEnabled(String provider) {

			Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

	}

	// Calculation can't be aborted
	@Override
	public void onBackPressed() {
		if (calculationRunning) {
			Toast.makeText(this, getResources().getString(R.string.calculate_rout_toast_noabortion), Toast.LENGTH_LONG).show();
		} else {
			super.onBackPressed();
		}

	}

}
