/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.view;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import at.bac.servicetour.R;
import at.bac.servicetour.controller.ClientController;
import at.bac.servicetour.domain.ClientDomain;

/** Pick Client Activity
 * 
 * Displays a list where the user can chose a client.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class PickClientActivity extends ListActivity {

	private static final String TAG = PickClientActivity.class.getSimpleName();

	// DB helper
	private ClientController cC = new ClientController();

	// Activity elements
	private HashMap<Long, String> clientHashMap;
	private ListView lv;

	public static final String CLIENT_ID = "CLIENT_ID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		lv = getListView();
		lv.setTextFilterEnabled(true);

		buildList();

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (id != -1) {
					final Intent intent = new Intent();
					intent.putExtra(CLIENT_ID, clientHashMap.get(id));
					setResult(Activity.RESULT_OK, intent);
					finish();
				}
			}
		});
	}

	/** Build List
	 * 
	 * This method fills the list with the required information.
	 * 
	 */
	private void buildList() {

		ArrayList<ClientDomain> clients = cC.getAllClients();
		final String[] columns = { "Zeile1", "Zeile2" };
		final String[] matrix = { "_id", "Zeile1", "Zeile2" };
		final int[] layouts = new int[] { android.R.id.text1, android.R.id.text2 };
		long key = 0;

		MatrixCursor cursor = new MatrixCursor(matrix);

		if (clients.isEmpty()) {
			key = -1;
			cursor.addRow(new Object[] { key, getResources().getString(R.string.clients_text_noClientsinDB), "" });
		} else {
			this.clientHashMap = new HashMap<Long, String>();
			for (ClientDomain c : clients) {
				// Log.d(TAG, "KEY: " + key);
				key = key + 1;
				this.clientHashMap.put(key, c.id);
				cursor.addRow(new Object[] { key, c.lastName + " " + c.firstName, c.street + " " + c.houseNumber + ", " + c.zip + " " + c.city });
			}
		}

		SimpleCursorAdapter data = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columns, layouts);

		setListAdapter(data);
	}

}
