/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour;

import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import at.bac.servicetour.view.MainActivity;

import com.couchbase.libcouch.AndCouch;
import com.couchbase.libcouch.CouchDB;
import com.couchbase.libcouch.ICouchClient;

public class Main extends Activity {

	private static final String TAG = Main.class.getSimpleName();

	public static String couchDBUrl;
	public static String dbName = "servicetour";

	String release = "release-0.1";
	ServiceConnection couchServiceConnection;

	private ProgressDialog installProgress;

	private boolean couchDBstarted = false;

	private final ICouchClient mCallback = new ICouchClient.Stub() {

		@Override
		public void couchStarted(String host, int port) {
			if (installProgress != null) {
				installProgress.dismiss();
			}

			couchDBUrl = "http://" + host + ":" + port + "/";

			try {
				AndCouch.put(couchDBUrl + dbName + "/", "");
				ensureDesignDoc();
				launchServiceTour();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void installing(int completed, int total) throws RemoteException {
			if (installProgress == null) {
				installProgress = new ProgressDialog(Main.this);
				installProgress.setTitle("Installing ServiceTour");
				installProgress.setCancelable(false);
				installProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				installProgress.show();
			}
			installProgress.setProgress(completed);
			installProgress.setMax(total);

		}

		@Override
		public void exit(String error) {
		}

		private void ensureDesignDoc() {

			String designDocString = "{\"_id\": \"_design/queries\",\"views\": {\"getAllClients\": {\"map\": \"function(doc){ if(doc.lastName) { emit(doc.lastName, doc)}}\"}, \"getAllAppointmentsByDate\": {\"map\": \"function(doc){ if(doc.from) { emit(doc.from, doc)}}\"},\"getAllAppointmentsByClient\": {\"map\": \"function(doc){ if(doc.from) { emit(doc.clientId, doc)}}\"}},\"language\": \"javascript\"}";

			try {
				AndCouch.put(couchDBUrl + dbName + "/_design/servicetour/", designDocString);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.startscreen);

		couchServiceConnection = CouchDB.getService(getBaseContext(), null, release, mCallback);

	}

	public void launchServiceTour() {

		Intent intentMainActivity = new Intent(this, MainActivity.class);
		startActivity(intentMainActivity);
	}

	/*
	 * This will be called when app comes back from the background, in the case
	 * that CouchDB had to be installed prior to launching, this will attempt to
	 * relaunch it.
	 */
	@Override
	public void onResume() {
		super.onResume();
		if (couchDBstarted) {
			finish();
		} else {
			couchDBstarted = true;
		}
	}

	/*
	 * When the app is killed we unbind from CouchDB, so it can stop itself if
	 * not used
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			unbindService(couchServiceConnection);
			Log.i(TAG, "--------Service CouchDB unbinded--------");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
}