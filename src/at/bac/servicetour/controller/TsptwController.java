package at.bac.servicetour.controller;

import java.util.GregorianCalendar;

import at.bac.servicetour.model.Tsptw;

public class TsptwController implements ITsptwController {

	@Override
	public String getResult(GregorianCalendar calStart, GregorianCalendar calEnd, String gpsPos) {
		Tsptw tsptw = new Tsptw(calStart, calEnd, gpsPos);
		return tsptw.result;
	}

}
