package at.bac.servicetour.controller;

import at.bac.servicetour.model.CouchDBHandler;

public class DatabaseController implements IDatabaseController {

	CouchDBHandler dbHandler;
	
	public DatabaseController() {
		dbHandler = new CouchDBHandler();
	}
	
	@Override
	public boolean syncPush(String userName, String pw, String server, String database) {
		return dbHandler.syncPush(userName, pw, server, database);
	}

	@Override
	public boolean syncPull(String userName, String pw, String server, String database) {
		return dbHandler.syncPull(userName, pw, server, database);
	}

}
