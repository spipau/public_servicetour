package at.bac.servicetour.controller;

import java.util.GregorianCalendar;

public interface ITsptwController {

	public String getResult(GregorianCalendar calStart, GregorianCalendar calEnd, String gpsPos);

}
