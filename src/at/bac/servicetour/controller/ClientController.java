/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.controller;

import java.util.ArrayList;

import at.bac.servicetour.domain.ClientDomain;
import at.bac.servicetour.model.CouchDBHandler;
import at.bac.servicetour.model.IDBHandler;

public class ClientController implements IClientController {

	IDBHandler dbHandler;	
	
	public ClientController() {
		dbHandler = new CouchDBHandler();
	}
	
	public ArrayList<ClientDomain> getAllClients() {
		return dbHandler.getAllClients();
	}

	public boolean createNewClient(ClientDomain client) {
		
		return dbHandler.createClient(client);
	}

	public ClientDomain getClientByID(String id) {
		return dbHandler.getClient(id);
	}

	public boolean deleteClientById(String id, String rev) {
		
		return dbHandler.deleteClient(id, rev);
	}

	public boolean updateClientById(ClientDomain client) {
		
		return dbHandler.updateClient(client);
	}

}
