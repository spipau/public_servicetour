/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.controller;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import at.bac.servicetour.domain.AppointmentDomain;


public interface IAppointmentController {

	public boolean createAppointment(AppointmentDomain appointment);
	public boolean updateAppointment(AppointmentDomain appointment);
	public boolean deleteAppointment(String id, String rev);
	public AppointmentDomain getAppointment(String id);
	public ArrayList<AppointmentDomain> getAllAppointmentsByDate(GregorianCalendar cal);
	public ArrayList<AppointmentDomain> getAllAppointmentsByClient(String clientId);
	public ArrayList<AppointmentDomain> getAllFutureAppointmentsByClient(String clientId);
	public ArrayList<AppointmentDomain> getAllPastAppointmentsByClient(String clientId);
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateCompleted(GregorianCalendar cal);
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateUncompleted(GregorianCalendar cal);
}
