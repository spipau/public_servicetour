package at.bac.servicetour.controller;

public interface IDatabaseController {

	public boolean syncPush(String userName, String pw, String server, String database);
	public boolean syncPull(String userName, String pw, String server, String database);
}
