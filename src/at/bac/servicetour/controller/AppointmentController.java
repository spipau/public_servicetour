/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.controller;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.model.CouchDBHandler;
import at.bac.servicetour.model.IDBHandler;


public class AppointmentController implements IAppointmentController {

	IDBHandler dbHandler;
	
	public AppointmentController() {
		dbHandler = new CouchDBHandler();
	}

	@Override
	public boolean createAppointment(AppointmentDomain appointment) {
		
		return dbHandler.createAppointment(appointment);
	}

	@Override
	public boolean updateAppointment(AppointmentDomain appointment) {
		
		return dbHandler.updateAppointment(appointment);
	}

	@Override
	public boolean deleteAppointment(String id, String rev) {
		
		return dbHandler.deleteAppointment(id, rev);
	}

	@Override
	public AppointmentDomain getAppointment(String id) {
		
		return dbHandler.getAppointment(id);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByClient(String clientId) {
		
		return dbHandler.getAllAppointmentsByClient(clientId);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDate(GregorianCalendar cal) {
		
		return dbHandler.getAllAppointmentsByDate(cal);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllFutureAppointmentsByClient(
			String clientId) {
		
		return dbHandler.getAllFutureAppointmentsByClient(clientId);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllPastAppointmentsByClient(
			String clientId) {
		
		return dbHandler.getAllPastAppointmentsByClient(clientId);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateCompleted(
			GregorianCalendar cal) {

		return dbHandler.getAllAppointmentsByDateCompleted(cal);
	}

	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateUncompleted(
			GregorianCalendar cal) {

		return dbHandler.getAllAppointmentsByDateUncompleted(cal);
	}
	
	
}
