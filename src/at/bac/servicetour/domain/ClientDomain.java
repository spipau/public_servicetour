/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.domain;

/**
 * 
 * Domain-Class for client
 * 
 * @author Alexander Roessler
 * 
 */
public class ClientDomain {

	public String id;
	public String rev;
	public String lastName;
	public String firstName;
	public String street;
	public String houseNumber;
	public Integer zip;
	public String city;
	public String phone;
	public String mobil;
	public String note;

	public ClientDomain(String id, String rev, String lastName, String firstName, String street, String houseNumber, Integer zip, String city, String phone, String mobil, String note) {

		this.id = id;
		this.rev = rev;
		this.lastName = lastName;
		this.firstName = firstName;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.city = city;
		this.phone = phone;
		this.mobil = mobil;
		this.note = note;
	}

	public ClientDomain(String id, String lastName, String firstName, String street, String houseNumber, Integer zip, String city, String phone, String mobil, String note) {

		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.city = city;
		this.phone = phone;
		this.mobil = mobil;
		this.note = note;
	}

	public ClientDomain(String lastName, String firstName) {
		this.lastName = lastName;
		this.firstName = firstName;
	}

	public ClientDomain(String id, String lastName, String firstName, String street, String houseNumber, Integer zip, String city) {
		super();
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.city = city;
	}

	public ClientDomain(String id, String street, String houseNumber, Integer zip, String city) {
		super();
		this.id = id;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.city = city;
	}

	public ClientDomain() {
	}

	public ClientDomain(String id, String rev, String lastName, String firstName, String street, String houseNumber, Integer zip, String city) {
		super();
		this.id = id;
		this.rev = rev;
		this.lastName = lastName;
		this.firstName = firstName;
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.city = city;
	}

}
