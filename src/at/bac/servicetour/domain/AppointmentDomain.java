/* Copyright 2011 Roessler Alexander, Spiesberger-Hoeckner Alois Paul
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.bac.servicetour.domain;

import java.util.GregorianCalendar;

/**
 * 
 * Domain-Class for appointments
 * 
 * @author Alexander Roessler
 * 
 */
public class AppointmentDomain {

	public String id;
	public String rev;
	public GregorianCalendar from;
	public GregorianCalendar to;
	public int duration;
	public String clientId;
	public boolean isPrivate;
	public String note;
	public int number;
	public boolean completed;

	public AppointmentDomain(String id, String rev, GregorianCalendar from, GregorianCalendar to, int duration, String clientId, boolean isPrivate, String note, boolean completed) {

		this.id = id;
		this.rev = rev;
		this.from = from;
		this.to = to;
		this.duration = duration;
		this.clientId = clientId;
		this.isPrivate = isPrivate;
		this.note = note;
		this.completed = completed;
	}

	public AppointmentDomain(String id, String rev, GregorianCalendar from, GregorianCalendar to, int duration, String clientId, boolean isPrivate, boolean completed) {

		this.id = id;
		this.rev = rev;
		this.from = from;
		this.to = to;
		this.duration = duration;
		this.clientId = clientId;
		this.isPrivate = isPrivate;
		this.completed = completed;
	}

	public AppointmentDomain(GregorianCalendar from, GregorianCalendar to, int duration, String clientId, boolean isPrivate, String note) {

		this.from = from;
		this.to = to;
		this.duration = duration;
		this.clientId = clientId;
		this.isPrivate = isPrivate;
		this.note = note;
	}

	public AppointmentDomain(String id, GregorianCalendar from, GregorianCalendar to, int duration, String clientId, boolean isPrivate, String note) {

		this.id = id;
		this.from = from;
		this.to = to;
		this.duration = duration;
		this.clientId = clientId;
		this.isPrivate = isPrivate;
		this.note = note;
	}
}
