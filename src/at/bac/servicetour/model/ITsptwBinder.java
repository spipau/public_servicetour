package at.bac.servicetour.model;

/** Tsptw Binder Interface
 * 
 * The interface for the Tsptw binder, used for the connection between service and activity
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public interface ITsptwBinder {

	/** Optimize Route
	 * 
	 * Calculates the route.
	 * 
	 */
	public void optimizeRoute(); 
}
