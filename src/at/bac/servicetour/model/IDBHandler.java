package at.bac.servicetour.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/**
 * 
 * Interface for handling client- and appointment-data in database
 * 
 * @author Alexander Roessler
 * 
 */
public interface IDBHandler {

	/**
	 * 
	 * Create a new client in the database
	 * 
	 * @param client
	 * @return true if successful
	 */
	public boolean createClient(ClientDomain client);

	/**
	 * 
	 * Update client in database
	 * 
	 * @param client
	 * @return true if successful
	 */
	public boolean updateClient(ClientDomain client);

	/**
	 * 
	 * Delete client in database with given id and revision-id
	 * 
	 * @param id
	 * @param rev
	 * @return true if successful
	 */
	public boolean deleteClient(String id, String rev);

	/**
	 * 
	 * Get client form database with given id
	 * 
	 * @param id
	 * @return client
	 */
	public ClientDomain getClient(String id);

	/**
	 * 
	 * Get all clients from database
	 * 
	 * @return list of clients
	 */
	public ArrayList<ClientDomain> getAllClients();

	/**
	 * 
	 * Create a new appointment in the database
	 * 
	 * @param appointment
	 * @return true if successful
	 */
	public boolean createAppointment(AppointmentDomain appointment);

	/**
	 * 
	 * Update appointment in database
	 * 
	 * @param appointment
	 * @return true if successful
	 */
	public boolean updateAppointment(AppointmentDomain appointment);

	/**
	 * 
	 * Delete appointment in database with given id and revision-id
	 * 
	 * @param id
	 * @param rev
	 * @return true if successful
	 */
	public boolean deleteAppointment(String id, String rev);

	/**
	 * 
	 * Get appointment form database with given id
	 * 
	 * @param id
	 * @return appointment
	 */
	public AppointmentDomain getAppointment(String id);

	/**
	 * 
	 * Get all appointments with given date
	 * 
	 * @param cal
	 *            date of appointments
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllAppointmentsByDate(GregorianCalendar cal);

	/**
	 * 
	 * Get all appointments with given date which are completed
	 * 
	 * @param cal
	 *            date of appointments
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateCompleted(GregorianCalendar cal);

	/**
	 * 
	 * Get all appointments with given date which are uncompleted
	 * 
	 * @param cal
	 *            date of appointments
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateUncompleted(GregorianCalendar cal);

	/**
	 * 
	 * Get all appointments with given client-id
	 * 
	 * @param clientId
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllAppointmentsByClient(String clientId);

	/**
	 * 
	 * Get all future appointments with given client-id
	 * 
	 * @param clientId
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllFutureAppointmentsByClient(String clientId);

	/**
	 * 
	 * Get all past appointments with given client-id
	 * 
	 * @param clientId
	 * @return list of appointments
	 */
	public ArrayList<AppointmentDomain> getAllPastAppointmentsByClient(String clientId);

	/**
	 * 
	 * Get a Universally Unique Identifier
	 * 
	 * @return uuid
	 */
	public String getUUID();
	
	/** 
	 * 
	 * Synchronizes the CouchDB with a Server. Pushes the Data to the Server.
	 * 
	 * @return True if the connection was successful, else false.
	 */
	public boolean syncPush(String userName, String pw, String server, String database);
	
	/** 
	 * 
	 * Synchronizes the CouchDB with a Server. Pulls the Data from the Server.
	 * 
	 * @return True if the connection was successful, else false.
	 */
	public boolean syncPull(String userName, String pw, String server, String database);
}
