package at.bac.servicetour.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import at.bac.servicetour.controller.ITsptwController;
import at.bac.servicetour.controller.TsptwController;
import at.bac.servicetour.view.SettingsActivity;

/** Tsptw Service
 * 
 * This service is stated when a calculation of a route is needed. So the program is not freezing.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class TsptwService extends Service {

	private static final String TAG = TsptwService.class.getSimpleName();

	// Callback
	private Handler mCallbackHandler;
	// Binder
	private final IBinder mTstpwBinder = new TsptwBinder();

	// Information for the calculation
	public static int day2cal;
	public static int month2cal;
	public static int year2cal;
	public static String gpsPos;

	// Controller
	private ITsptwController tsptwController;
	
	/** Tsptw Binder
	 * 
	 * The binder is used for the connection between the activity and the service.
	 * 
	 * @author Alois Paul Spiesberger-H�ckner
	 *
	 */
	public class TsptwBinder extends Binder implements ITsptwBinder {

		/** Set Activity Callback Handler
		 * 
		 * Sets the callback handler, so the service is able to send messages to the activity.
		 * 
		 * @param callback The callback object.
		 */
		public void setActivityCallbackHandler(final Handler callback) {
			mCallbackHandler = callback;
		}


		/** Set Data for Calculation
		 * 
		 * Sets all needed information for the calculation.
		 * 
		 * @param day The day.
		 * @param month The month.
		 * @param year The year.
		 * @param gpsPos The GPS position of the current position.
		 */
		public void setDataForTsptwCalculation(int day, int month, int year, String gpsPos) {
			TsptwService.day2cal = day;
			TsptwService.month2cal = month;
			TsptwService.year2cal = year;
			TsptwService.gpsPos = gpsPos;
		}

		@Override
		public void optimizeRoute() {
			new Thread() {

				public void run() {
					String succeeded = _optimizeRoute();

					final Message msg = new Message();
					final Bundle bundle = new Bundle();
					bundle.putString("optimizedSucceeded", succeeded);
					msg.setData(bundle);
					mCallbackHandler.sendMessage(msg);
				}

			}.start();
		}
	}

	/** Optimize Route
	 * 
	 * Calculates the optimized route.
	 * 
	 * @return The result of the calculation. The string contains the necessary IDs. All IDs are separated by one whitespace:
	 */
	private String _optimizeRoute() {

		SharedPreferences settings = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		
		GregorianCalendar calStart = new GregorianCalendar(this.year2cal, this.month2cal, this.day2cal);
		
		GregorianCalendar calEnd = new GregorianCalendar();
		calEnd.set(Calendar.HOUR_OF_DAY, settings.getInt("mHourTo", 8));
		calEnd.set(Calendar.MINUTE, settings.getInt("mMinuteTo", 0));
		
		GregorianCalendar calToday = new GregorianCalendar();
		
		if(calToday.get(Calendar.YEAR) == this.year2cal && calToday.get(Calendar.MONTH) == this.month2cal && calToday.get(Calendar.DAY_OF_MONTH) == this.day2cal) {
			calStart.set(Calendar.HOUR_OF_DAY, new GregorianCalendar().get(Calendar.HOUR_OF_DAY));
			calStart.set(Calendar.MINUTE, new GregorianCalendar().get(Calendar.MINUTE));
			calStart.set(Calendar.SECOND, 0);
		} else {
			calStart.set(Calendar.HOUR_OF_DAY, settings.getInt("mHourTo", 8));
			calStart.set(Calendar.MINUTE, settings.getInt("mMinuteTo", 0));
			calStart.set(Calendar.SECOND, 0);
		}
		
//		Log.e(TAG, "Year:" + calStart.get(Calendar.YEAR));
//		Log.e(TAG, "month:" + calStart.get(Calendar.MONTH));
//		Log.e(TAG, "day:" + calStart.get(Calendar.DAY_OF_MONTH));
//		Log.e(TAG, "hour:" + calStart.get(Calendar.HOUR_OF_DAY));
//		Log.e(TAG, "minute:" + calStart.get(Calendar.MINUTE));
//		Log.e(TAG, "sec:" + calStart.get(Calendar.SECOND));
		
//		Tsptw tsptw = new Tsptw(calStart, calEnd, this.gpsPos);
		

		// Log.d(TAG, "******************************* " + tsptw.result);

		return this.tsptwController.getResult(calStart, calEnd, this.gpsPos);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return this.mTstpwBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		this.tsptwController = new TsptwController();
		Log.i(TAG, "Service started ...");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "*********************\nSERVICE END\n*********************");
	}
}
