package at.bac.servicetour.model;

/** Sync Binder Interface
 * 
 * The interface for the sync binder, used for the connection between service and activity
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public interface ISyncCouchDBBinder {

	/** Pull
	 * 
	 * Pulls data from the server
	 */
	public void pull();
	
	/** Push
	 * 
	 * Pushes data to the server
	 */
	public void push();
}
