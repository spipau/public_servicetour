package at.bac.servicetour.model;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class MapFunctions {

	private final String TAG = MapFunctions.class.getSimpleName();
	private int timeToDestination;
	private int distanceToDestination;

	/**
	 * Distance and time
	 * 
	 * Calculates the distance and the time (by car) between two addresses
	 * (start and destination) The first value in the int[] Array is the time to
	 * destination and the second one is the distance to the destination.
	 * 
	 * @param start
	 *            Start address
	 * @param destination
	 *            Destination address
	 * @return int[] filled with time to destination and distance to destination
	 */
	public int[] getDistanceAndTime(String start, String destination) {

		String url = "http://maps.google.at/maps/api/directions/json?origin=" + start + "&destination=" + destination + "&sensor=false";

		String result = "";
		InputStream is = null;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http conection" + e.toString());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {
			JSONObject rootObj = new JSONObject(result);

			if (rootObj.getString("status").equals("OK")) {
				JSONArray routes = (JSONArray) rootObj.get("routes");

				if (routes.length() < 1)
					Log.e(TAG, "ERROR no legs there");
				JSONObject firstRoute = routes.getJSONObject(0);
				JSONArray legs = (JSONArray) firstRoute.get("legs");
				if (legs.length() < 1)
					Log.e(TAG, "ERROR no legs there");
				JSONObject firstLeg = legs.getJSONObject(0);
				JSONObject durationObject = (JSONObject) firstLeg.get("duration");
				JSONObject distanceObject = (JSONObject) firstLeg.get("distance");

				// finally we will get the values distance in meters and time in
				// seconds!!
				timeToDestination = (Integer) durationObject.get("value") / 60;
				distanceToDestination = (Integer) distanceObject.get("value");
			}

		} catch (JSONException e) {
			Log.e("log_tag", "Error read JSON" + e.toString());
			e.printStackTrace();
		}

		return new int[] { timeToDestination, distanceToDestination };
	}

	/**
	 * 
	 * check given address if it exists. source of this information is
	 * Google-maps
	 * 
	 * @param address
	 * @return true if successful
	 */
	public boolean validateAddress(String address) {

		Log.d(TAG, "Start Validation");

		String url = "http://maps.google.at/maps/api/geocode/json?address=" + address + "+österreich&sensor=false";

		String result = "";
		InputStream is = null;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http conection" + e.toString());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {
			JSONObject rootObj = new JSONObject(result);

			if (rootObj.get("status").equals("OK")) {
				Log.d(TAG, "End Validation TRUE");
				return true;
			} else {
				Log.d(TAG, "End Validation FALSE");
				return false;
			}
		} catch (JSONException e) {
			Log.e("log_tag", "Error get JSON Response " + e.getMessage());
			return false;
		}
	}
}
