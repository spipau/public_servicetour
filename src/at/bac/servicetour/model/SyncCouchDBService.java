package at.bac.servicetour.model;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import at.bac.servicetour.controller.DatabaseController;
import at.bac.servicetour.controller.IDatabaseController;

/** Synchronization Service
 * 
 * This service is stated when a synchronization with an other CouchDB is needed. So the program is not freezing.
 * 
 * @author Alois Paul Spiesberger-H�ckner
 *
 */
public class SyncCouchDBService extends Service {

	// database
	private IDatabaseController dC;
	
	// Callbackhandler
	private Handler mCallbackHandler;

	// Information for the synchronization
	private static String userName;
	private static String pw;
	private static String server;
	private static String database;
	
	private static final String TAG = SyncCouchDBService.class.getSimpleName();
	
	// Binder
	private SyncCouchDBBinder mSyncCouchDBBinder;
	
	@Override
	public void onCreate() {
		super.onCreate();
		mSyncCouchDBBinder = new SyncCouchDBBinder();
		dC = new DatabaseController();
		Log.i(TAG, "Service started ...");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();	
		Log.i(TAG, "*********************\nSERVICE END\n*********************");
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return mSyncCouchDBBinder;
	}

	/** Pull
	 * 
	 * Pulls the couchDB database from the server
	 * 
	 * @return "ok" if the connection was successful otherwise "fail".
	 */
	private String _pull() {
		if(dC.syncPull(userName, pw, server, database)) {
			return "ok";
		} else {
			return "fail";
		}
	}

	/** Push
	 * 
	 * Pushes the couchDB database to the server
	 * 
	 * @return "ok" if the connection was successful otherwise "fail".
	 */
	private String _push() {
		if(dC.syncPush(userName, pw, server, database)) {
			return "ok";
		} else {
			return "fail";
		}
	}
	
	/** Synchronization Binder
	 * 
	 * The binder is used for the connection between the activity and the service.
	 * 
	 * @author Alois Paul Spiesberger-H�ckner
	 *
	 */
	public class SyncCouchDBBinder extends Binder implements ISyncCouchDBBinder {

		/** Set Activity Callback Handler
		 * 
		 * Sets the callback handler, so the service is able to send messages to the activity.
		 * 
		 * @param callback The callback object.
		 */
		public void setActivityCallbackHandler(final Handler callback) {
			mCallbackHandler = callback;
		}


		/** Set Data for Synchronization
		 * 
		 * Sets all needed information for the synchronization.
		 * 
		 * @param userName The user name for the login at the couchDB
		 * @param pw The password for the login at the couchDB
		 * @param server The URL of the server
		 * @param database The name of the database
		 */
		public void setDataForSync(String userName, String pw, String server, String database) {
			SyncCouchDBService.userName = userName;
			SyncCouchDBService.pw = pw;
			SyncCouchDBService.server = server;
			SyncCouchDBService.database = database;
		}

		@Override
		public void pull() {

			new Thread() {

				public void run() {

					String result = _pull();

					final Message msg = new Message();
					final Bundle bundle = new Bundle();
					bundle.putString("message", result);
					msg.setData(bundle);
					mCallbackHandler.sendMessage(msg);

				}

			}.start();

		}


		@Override
		public void push() {

			new Thread() {

				public void run() {

					String result = _push();

					final Message msg = new Message();
					final Bundle bundle = new Bundle();
					bundle.putString("message", result);
					msg.setData(bundle);
					mCallbackHandler.sendMessage(msg);

				}

			}.start();
			
		}
		
	}
}
