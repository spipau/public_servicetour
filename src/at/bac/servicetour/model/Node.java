package at.bac.servicetour.model;

import java.util.GregorianCalendar;

import at.bac.servicetour.domain.AppointmentDomain;

/**
 * 
 * Class Node is needed for Tsptw-algorithm to save nodes in the tree
 * 
 * @author Alexander Roessler
 * 
 */
public class Node {

	public Node parent;
	public AppointmentDomain appointment;
	public GregorianCalendar finish;
	public Integer drivingTime;
	public Float drivingDistance;

	public Node(Node parent, AppointmentDomain appointment, GregorianCalendar finish, Integer drivingTime, Float drivingDistance) {
		super();
		this.parent = parent;
		this.appointment = appointment;
		this.finish = finish;
		this.drivingTime = drivingTime;
		this.drivingDistance = drivingDistance;
	}
}
