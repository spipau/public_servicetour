package at.bac.servicetour.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/**
 * Algorithm for traveling salesman problem with time windows Calculates all
 * possible routes for an appointment-list and finds fastest route
 * 
 * @author Alexander Roessler
 * 
 */
public class Tsptw {

	private static final String TAG = Tsptw.class.getSimpleName();

	private ArrayList<AppointmentDomain> appointments;
	private ArrayList<ClientDomain> clients = new ArrayList<ClientDomain>();
	private ArrayList<String> addressList = new ArrayList<String>();
	// private ArrayList<Node> leaves = new ArrayList<Node>();
	private Node shortestRoute = null;
	private Integer[][] drivingTime;
	private Integer[][] drivingDistance;
	private String errorMsg = "";

	/** List of end-points for possible variations */
	public String result = "";

	/**
	 * 
	 * Constructor creates a list of appointments of given day and calculates
	 * all possible routes after that they will be stored in an array-list
	 * (leaves)
	 * 
	 * @param date
	 *            to get all appointments of this day
	 */
	public Tsptw(GregorianCalendar startTime, GregorianCalendar endTime, String gpsPos) {

		/* Only for testing */
		// startTime.set(Calendar.HOUR_OF_DAY, 8);
		// startTime.set(Calendar.MINUTE, 0);

		long dLoadData;
		long dDistanceMatrix;
		long dAlgorithm;

		Log.i(TAG, "Traveler Salesman algorithm started");

		long sysStartTime = System.currentTimeMillis();

		// INFO: Endzeit wird zur Zeit nicht berücksichtigt, da man am Ergebnis
		// sieht wann die Route fertig ist.

		MapFunctions map = new MapFunctions();
		IDBHandler handler = new CouchDBHandler();
		appointments = handler.getAllAppointmentsByDateUncompleted(startTime);

		Log.i(TAG, "- Amount of Appointments: " + appointments.size());

		new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?latlng=" + gpsPos));

		AppointmentDomain rootAppointment = new AppointmentDomain("root", (GregorianCalendar) startTime.clone(), (GregorianCalendar) startTime.clone(), 0, null, false, "");
		appointments.add(0, rootAppointment);

		// root of the tree
		Node root = new Node(null, rootAppointment, rootAppointment.from, 0, 0f);

		drivingTime = new Integer[appointments.size()][appointments.size()];
		drivingDistance = new Integer[appointments.size()][appointments.size()];

		int nr = 0;

		Log.i(TAG, "- Start to get clients from appointments");
		// get clients from appointments
		for (AppointmentDomain appointment : appointments) {

			Log.i(TAG, "- Add appointment with ID: " + appointment.id);

			appointment.number = nr;

			if (!appointment.id.equals("root")) {
				clients.add(handler.getClient(appointment.clientId));
				Log.i(TAG, "- Add client with ID: " + appointment.clientId);
			} else {
				ClientDomain rootClient = new ClientDomain("root", "", "", 0, "" + "");
				clients.add(rootClient);
			}

			nr++;
		}

		Log.i(TAG, "- Get clients from appointments finished");

		addressList.add(gpsPos);

		// get address from clients
		for (ClientDomain client : clients) {
			if (!client.id.equals("root")) {
				String address = client.street + " " + client.houseNumber + " " + client.zip + " " + client.city;
				Log.i(TAG, "- Add address: " + address);
				addressList.add(address.replace(" ", "+"));
			}
		}

		int i = 0;
		int j = 0;

		dLoadData = System.currentTimeMillis() - sysStartTime;
		sysStartTime = System.currentTimeMillis();

		Log.i(TAG, "- Create driving-distance matrix");
		// create driving-time and driving-distance matrix
		for (AppointmentDomain appointmentFrom : appointments) {
			for (AppointmentDomain appointmentTo : appointments) {
				if (appointmentFrom.id != appointmentTo.id) {

					int[] data = map.getDistanceAndTime(addressList.get(i), addressList.get(j));

					Integer dT = data[0];
					Integer dD = data[1];

					if (dT != null && dD != null) {
						drivingTime[i][j] = dT;
						drivingDistance[i][j] = dD;
					} else {
						Log.e(TAG, "- To many requests to google maps api");
						errorMsg += "ERROR: Anzahl der Suchanfragen bei Google Maps überschritten. Sie müssen leider bis Morgen warten.";
					}

				} else {
					drivingTime[i][j] = -1;
				}
				j++;
			}
			j = 0;
			i++;
		}
		Log.i(TAG, "- Create driving-distance matrix finished");

		dDistanceMatrix = System.currentTimeMillis() - sysStartTime;

		// start algorithm only if there are no errors
		if (errorMsg.equals("")) {

			if (Log.isLoggable(TAG, Log.INFO)) {
				// Output Driving Time Matrix
				Log.d(TAG, "Driving Time Matrix");
				String output = "";
				for (i = 0; i < drivingTime.length; i++) {
					for (j = 0; j < drivingTime.length; j++) {
						output += drivingTime[i][j] + " ";
					}
					Log.d(TAG, output);
					output = "";
				}
			}

			appointments.remove(0);

			Log.i(TAG, "- Start to build tree");

			sysStartTime = System.currentTimeMillis();

			// start to build tree
			buildTree(root, appointments);

			dAlgorithm = System.currentTimeMillis() - sysStartTime;

			Log.i(TAG, "- building tree finished");

			if (shortestRoute != null) {
				StringBuffer weg = new StringBuffer();

				while (shortestRoute.parent != null) {
					weg.append(shortestRoute.appointment.id + " ");
					shortestRoute = shortestRoute.parent;
				}

				result = weg.toString();
			}

			Log.i(TAG, "Duration: ");
			Log.i(TAG, "Load Data:       " + dLoadData);
			Log.i(TAG, "Distance Matrix: " + dDistanceMatrix);
			Log.i(TAG, "Algorithm: " + dAlgorithm);

		} else {
			result = errorMsg;
		}

		Log.i(TAG, "- Shortest route: " + result);

		Log.i(TAG, "Traveler Salesman algorithm finished");
	}

	/**
	 * Recursive method for algorithm Creates Tree with all possible variations
	 * and finds fastest Route
	 * 
	 * @param node
	 *            current node
	 * @param missing
	 *            missing appointments in this fork
	 */
	public void buildTree(Node node, ArrayList<AppointmentDomain> missing) {

		int i = 0;

		for (AppointmentDomain appointment : missing) {

			@SuppressWarnings("unchecked")
			ArrayList<AppointmentDomain> rest = (ArrayList<AppointmentDomain>) missing.clone();

			GregorianCalendar arrival = calcArrivalTime(node, appointment);

			GregorianCalendar arrivalPlusDuration = (GregorianCalendar) arrival.clone();
			arrivalPlusDuration.set(GregorianCalendar.MINUTE, arrivalPlusDuration.get(GregorianCalendar.MINUTE) + appointment.duration);

			/*
			 * three options:
			 * 
			 * 1. arrival-time is in time-window: appointment will be finished
			 * at arrival-time + duration
			 * 
			 * 2. arrival-time is before time-window: appointment will be
			 * finished at start-time + duration
			 * 
			 * 3. arrival-time is after time-window: do nothing (this branch of
			 * the tree isn't a possible route)
			 */
			if ((arrival.after(appointment.from) || arrival.equals(appointment.from)) && (arrivalPlusDuration.before(appointment.to) || arrivalPlusDuration.equals(appointment.to))) {

				GregorianCalendar newFinish = (GregorianCalendar) node.finish.clone();

				newFinish.set(GregorianCalendar.MINUTE, newFinish.get(GregorianCalendar.MINUTE) + appointment.duration + drivingTime[node.appointment.number][appointment.number]);

				Node newNode = new Node(node, appointment, newFinish, node.drivingTime + drivingTime[node.appointment.number][appointment.number], node.drivingDistance + drivingDistance[node.appointment.number][appointment.number]);
				rest.remove(i);

				// if there are no further appointments this appointment is a
				// leave of the tree
				if (!rest.isEmpty()) {
					buildTree(newNode, rest);
				} else {
					findFastestRoute(newNode);
					Log.d(TAG, "- New Node found, appointmentID: " + newNode.appointment.id + " End-Time: " + newNode.finish.getTime().toString() + " Distance: " + newNode.drivingDistance);
				}

			} else if (arrival.before(appointment.from) || arrival.equals(appointment.from)) {

				GregorianCalendar newFinish = (GregorianCalendar) appointment.from.clone();

				// calculate end-time of this appointment
				newFinish.set(GregorianCalendar.MINUTE, newFinish.get(GregorianCalendar.MINUTE) + appointment.duration);

				Node newNode = new Node(node, appointment, newFinish, node.drivingTime + drivingTime[node.appointment.number][appointment.number], node.drivingDistance + drivingDistance[node.appointment.number][appointment.number]);
				rest.remove(i);

				// if there are no further appointments this appointment is a
				// leave of the tree
				if (!rest.isEmpty()) {
					buildTree(newNode, rest);
				} else {
					findFastestRoute(newNode);
					Log.d(TAG, "- New Node found, appointmentID: " + newNode.appointment.id + " End-Time: " + newNode.finish.getTime().toString() + " Distance: " + newNode.drivingDistance);
				}
			}

			i++;
		}
	}

	/**
	 * 
	 * Calculate Arrival-Time for next Appointment
	 * 
	 * @param currentNode
	 * @param appointment
	 * @return Arrival-Time
	 */
	private GregorianCalendar calcArrivalTime(Node currentNode, AppointmentDomain appointment) {
		GregorianCalendar arrival = (GregorianCalendar) currentNode.finish.clone();
		arrival.set(GregorianCalendar.MINUTE, arrival.get(GregorianCalendar.MINUTE) + drivingTime[currentNode.appointment.number][appointment.number]);
		return arrival;
	}

	/**
	 * 
	 * Compare fastestNode with newNode and finds faster one. If end-time is the
	 * same, then it finds the shorter way.
	 * 
	 * @param newNode
	 */
	private void findFastestRoute(Node newNode) {

		if (shortestRoute == null) {
			shortestRoute = newNode;
		} else {
			if (newNode.finish.before(shortestRoute.finish)) {
				shortestRoute = newNode;
			} else if (newNode.finish.equals(shortestRoute.finish)) {
				if (newNode.drivingTime < shortestRoute.drivingTime) {
					shortestRoute = newNode;
				} else if (newNode.drivingTime == shortestRoute.drivingTime) {
					if (newNode.drivingDistance <= shortestRoute.drivingDistance) {
						shortestRoute = newNode;
					}
				}
			}
		}
	}
}
