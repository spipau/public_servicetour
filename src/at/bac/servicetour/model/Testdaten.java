package at.bac.servicetour.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

/**
 * 
 * Creates test-data in Database
 * 
 * @author Alexander Roessler
 * 
 */
public class Testdaten {

	public Testdaten() {

		IDBHandler handler = new CouchDBHandler();

		ArrayList<ClientDomain> clients = new ArrayList<ClientDomain>();
		ArrayList<AppointmentDomain> appointments = new ArrayList<AppointmentDomain>();

		String[] uuidClients = new String[10];
		String[] uuidAppointments = new String[10];

		for (int i = 0; i < 10; i++) {
			uuidClients[i] = handler.getUUID();
		}

		for (int i = 0; i < 10; i++) {
			uuidAppointments[i] = handler.getUUID();
		}

		clients.add(new ClientDomain(uuidClients[0], "Mustermann", "Max", "Hasnerstra�e", "22", 1160, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[1], "M�ller", "Alois", "Siebenbrunnengasse", "7", 1050, "Wien", "0123456", "0676123456", "bla bla bla bla"));
		clients.add(new ClientDomain(uuidClients[2], "Maier", "Franz", "Herrengasse", "3", 1010, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[3], "Huber", "Karl", "Neubaugasse", "10", 1070, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[4], "Fischer", "Josef", "Johannesgasse", "5", 1010, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[5], "Bauer", "Fred", "B�rsegasse", "4", 1010, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[6], "Haberl", "Martin", "Singerstra�e", "7", 1050, "Wien", "0123456", "0676123456", "bla bla bla bla"));
		clients.add(new ClientDomain(uuidClients[7], "K�hler", "Manfred", "Herrengasse", "1", 1010, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[8], "Hauer", "Jakob", "Marxergasse", "5", 1030, "Wien", "0123456", "0676123456", "notiztext"));
		clients.add(new ClientDomain(uuidClients[9], "D�rfler", "Franz", "Schwedenplatz", "3", 1010, "Wien", "0123456", "0676123456", "notiztext"));

		// for (int i = 0; i < 10; i++) {
		// GregorianCalendar from = new GregorianCalendar();
		// GregorianCalendar to = new GregorianCalendar();
		//
		// from.set(GregorianCalendar.HOUR_OF_DAY, 12);
		// from.set(GregorianCalendar.MINUTE, 0);
		// from.set(GregorianCalendar.SECOND, 0);
		// to.set(GregorianCalendar.HOUR_OF_DAY, 20);
		// to.set(GregorianCalendar.MINUTE, 0);
		// to.set(GregorianCalendar.SECOND, 0);
		// appointments.add(new AppointmentDomain(uuidAppointments[i], from, to,
		// 10, uuidClients[i], false, "test test test"));
		// }

		GregorianCalendar from0 = new GregorianCalendar();
		GregorianCalendar to0 = new GregorianCalendar();
		GregorianCalendar from1 = new GregorianCalendar();
		GregorianCalendar to1 = new GregorianCalendar();
		GregorianCalendar from2 = new GregorianCalendar();
		GregorianCalendar to2 = new GregorianCalendar();
		GregorianCalendar from3 = new GregorianCalendar();
		GregorianCalendar to3 = new GregorianCalendar();
		GregorianCalendar from4 = new GregorianCalendar();
		GregorianCalendar to4 = new GregorianCalendar();

		from0.set(GregorianCalendar.HOUR_OF_DAY, 9);
		from0.set(GregorianCalendar.MINUTE, 0);
		from0.set(GregorianCalendar.SECOND, 0);
		to0.set(GregorianCalendar.HOUR_OF_DAY, 10);
		to0.set(GregorianCalendar.MINUTE, 0);
		to0.set(GregorianCalendar.SECOND, 0);
		appointments.add(new AppointmentDomain(uuidAppointments[0], from0, to0, 10, uuidClients[1], false, "test test test"));

		from1.set(GregorianCalendar.HOUR_OF_DAY, 10);
		from1.set(GregorianCalendar.MINUTE, 0);
		from1.set(GregorianCalendar.SECOND, 0);
		to1.set(GregorianCalendar.HOUR_OF_DAY, 11);
		to1.set(GregorianCalendar.MINUTE, 0);
		to1.set(GregorianCalendar.SECOND, 0);
		appointments.add(new AppointmentDomain(uuidAppointments[1], from1, to1, 10, uuidClients[1], false, "test test test"));

		from2.set(GregorianCalendar.HOUR_OF_DAY, 11);
		from2.set(GregorianCalendar.MINUTE, 0);
		from2.set(GregorianCalendar.SECOND, 0);
		to2.set(GregorianCalendar.HOUR_OF_DAY, 12);
		to2.set(GregorianCalendar.MINUTE, 0);
		to2.set(GregorianCalendar.SECOND, 0);
		appointments.add(new AppointmentDomain(uuidAppointments[2], from2, to2, 10, uuidClients[2], false, "test test test"));

		from3.set(GregorianCalendar.HOUR_OF_DAY, 12);
		from3.set(GregorianCalendar.MINUTE, 0);
		from3.set(GregorianCalendar.SECOND, 0);
		to3.set(GregorianCalendar.HOUR_OF_DAY, 13);
		to3.set(GregorianCalendar.MINUTE, 0);
		to3.set(GregorianCalendar.SECOND, 0);
		appointments.add(new AppointmentDomain(uuidAppointments[3], from3, to3, 10, uuidClients[3], false, "test test test"));

		from4.set(GregorianCalendar.HOUR_OF_DAY, 9);
		from4.set(GregorianCalendar.MINUTE, 0);
		from4.set(GregorianCalendar.SECOND, 0);
		to4.set(GregorianCalendar.HOUR_OF_DAY, 12);
		to4.set(GregorianCalendar.MINUTE, 0);
		to4.set(GregorianCalendar.SECOND, 0);
		appointments.add(new AppointmentDomain(uuidAppointments[4], from4, to4, 10, uuidClients[4], false, "test test test"));

		for (ClientDomain client : clients) {
			handler.createClient(client);
		}

		for (AppointmentDomain appointment : appointments) {
			handler.createAppointment(appointment);
		}
	}
}
