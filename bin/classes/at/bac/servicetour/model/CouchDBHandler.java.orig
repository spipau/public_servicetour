package at.bac.servicetour.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import at.bac.servicetour.Main;
import at.bac.servicetour.domain.AppointmentDomain;
import at.bac.servicetour.domain.ClientDomain;

import com.couchbase.libcouch.AndCouch;

/**
 * 
 * Implementation of @see IDBHandler
 * 
 * @author Alexander Roessler
 * 
 */
public class CouchDBHandler implements IDBHandler {

	private static final String tag = CouchDBHandler.class.getSimpleName();

	/**
	 * @see IDBHandler#createClient
	 */
	@Override
	public boolean createClient(ClientDomain client) {

		String jsonString = "{" + "\"firstName\":\"" + client.firstName + "\"," + "\"lastName\":\"" + client.lastName + "\"," + "\"street\":\"" + client.street + "\"," + "\"houseNumber\":\"" + client.houseNumber + "\"," + "\"zip\":\"" + client.zip + "\"," + "\"city\":\"" + client.city + "\"," + "\"phone\":\"" + client.phone + "\"," + "\"mobil\":\"" + client.mobil + "\"," + "\"note\":\"" + client.note + "\"" + "}";

		jsonString = asciiToISO8859(jsonString);

		if (client.id == null) {
			client.id = getUUID();
		}

		try {
			AndCouch req = AndCouch.put(Main.couchDBUrl + Main.dbName + "/" + client.id, jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "create client failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "create client failed");

		return false;
	}

	/**
	 * @see IDBHandler#updateClient
	 */
	@Override
	public boolean updateClient(ClientDomain client) {

		String jsonString = "{" + "\"_id\":\"" + client.id + "\"," + "\"_rev\":\"" + client.rev + "\"," + "\"firstName\":\"" + client.firstName + "\"," + "\"lastName\":\"" + client.lastName + "\"," + "\"street\":\"" + client.street + "\"," + "\"houseNumber\":\"" + client.houseNumber + "\"," + "\"zip\":\"" + client.zip + "\"," + "\"city\":\"" + client.city + "\"," + "\"phone\":\"" + client.phone + "\"," + "\"mobil\":\"" + client.mobil + "\"," + "\"note\":\"" + client.note + "\"" + "}";

		jsonString = asciiToISO8859(jsonString);

		try {
			AndCouch req = AndCouch.put(Main.couchDBUrl + Main.dbName + "/" + client.id, jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "update client failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "update client failed");

		return false;
	}

	/**
	 * @see IDBHandler#deleteClient
	 */
	@Override
	public boolean deleteClient(String id, String rev) {

		ArrayList<AppointmentDomain> appointments = getAllAppointmentsByClient(id);

		for (AppointmentDomain appointment : appointments) {

			deleteAppointment(appointment.id, appointment.rev);
		}

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/" + id + "?rev=" + rev;

		String[][] headers = { { "", "" } };

		try {
			AndCouch req = AndCouch.httpRequest("DELETE", ddocUrl, null, headers);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "delete client failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "delete client failed");

		return false;
	}

	/**
	 * @see IDBHandler#getClient
	 */
	@Override
	public ClientDomain getClient(String id) {

		ClientDomain client = null;
		AndCouch req;

		try {
			req = AndCouch.get(Main.couchDBUrl + Main.dbName + "/" + id);
			JSONObject responseObject = req.json;

			try {
				String rev = responseObject.getString("_rev");
				String lastName = iso8859ToAscii(responseObject.getString("lastName"));
				String firstName = iso8859ToAscii(responseObject.getString("firstName"));
				String street = iso8859ToAscii(responseObject.getString("street"));
				String houseNumber = iso8859ToAscii(responseObject.getString("houseNumber"));
				Integer zip = responseObject.getInt("zip");
				String city = iso8859ToAscii(responseObject.getString("city"));
				String phone = iso8859ToAscii(responseObject.getString("phone"));
				String mobil = iso8859ToAscii(responseObject.getString("mobil"));
				String note = iso8859ToAscii(responseObject.getString("note"));

				client = new ClientDomain(id, rev, lastName, firstName, street, houseNumber, zip, city, phone, mobil, note);

			} catch (JSONException e) {
				Log.e(tag, "get client failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get client failed: " + e1.getMessage());
		}

		return client;
	}

	/**
	 * @see IDBHandler#getAllClients
	 */
	@Override
	public ArrayList<ClientDomain> getAllClients() {

		ArrayList<ClientDomain> clientList = new ArrayList<ClientDomain>();

		try {
			AndCouch req = AndCouch.get(Main.couchDBUrl + Main.dbName + "/_design/servicetour/_view/getAllClients");
			JSONObject responseObject = req.json;
			try {
				if (responseObject.getInt("total_rows") > 0) {

					JSONArray array = responseObject.getJSONArray("rows");

					for (int i = 0; i <= array.length() - 1; i++) {

						JSONObject value = array.getJSONObject(i).getJSONObject("value");

						String id = value.getString("_id");
						String rev = value.getString("_rev");
						String lastName = iso8859ToAscii(value.getString("lastName"));
						String firstName = iso8859ToAscii(value.getString("firstName"));
						String street = iso8859ToAscii(value.getString("street"));
						String houseNumber = iso8859ToAscii(value.getString("houseNumber"));
						Integer zip = value.getInt("zip");
						String city = iso8859ToAscii(value.getString("city"));

						clientList.add(new ClientDomain(id, rev, lastName, firstName, street, houseNumber, zip, city));
					}
				}
			} catch (JSONException e) {
				Log.e(tag, "get all clients failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get all clients failed: " + e1.getMessage());
		}

		return clientList;
	}

	/**
	 * @see IDBHandler#createAppointment
	 */
	@Override
	public boolean createAppointment(AppointmentDomain appointment) {

		String jsonString = "{" + "\"from\":\"" + gregToString(appointment.from) + "\"," + "\"to\":\"" + gregToString(appointment.to) + "\"," + "\"duration\":\"" + appointment.duration + "\"," + "\"clientId\":\"" + appointment.clientId + "\"," + "\"isPrivate\":\"" + appointment.isPrivate + "\"," + "\"note\":\"" + appointment.note + "\"," + "\"completed\":\"" + appointment.completed + "\"" + "}";

		jsonString = asciiToISO8859(jsonString);

		if (appointment.id == null) {
			appointment.id = getUUID();
		}

		try {
			AndCouch req = AndCouch.put(Main.couchDBUrl + Main.dbName + "/" + appointment.id, jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "create appointment failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "create appointment failed");

		return false;
	}

	/**
	 * @see IDBHandler#updateAppointment
	 */
	@Override
	public boolean updateAppointment(AppointmentDomain appointment) {

		String jsonString = "{" + "\"_id\":\"" + appointment.id + "\"," + "\"_rev\":\"" + appointment.rev + "\"," + "\"from\":\"" + gregToString(appointment.from) + "\"," + "\"to\":\"" + gregToString(appointment.to) + "\"," + "\"duration\":\"" + appointment.duration + "\"," + "\"clientId\":\"" + appointment.clientId + "\"," + "\"isPrivate\":\"" + appointment.isPrivate + "\"," + "\"note\":\"" + appointment.note + "\"," + "\"completed\":\"" + appointment.completed + "\"" + "}";

		jsonString = asciiToISO8859(jsonString);

		try {
			AndCouch req = AndCouch.put(Main.couchDBUrl + Main.dbName + "/" + appointment.id, jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "update appointment failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "update appointment failed");

		return false;
	}

	/**
	 * @see IDBHandler#deleteAppointment
	 */
	@Override
	public boolean deleteAppointment(String id, String rev) {

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/" + id + "?rev=" + rev;

		String[][] headers = { { "", "" } };

		try {
			AndCouch req = AndCouch.httpRequest("DELETE", ddocUrl, null, headers);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "delete appointment failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "delete appointment failed");

		return false;
	}

	/**
	 * @see IDBHandler#getAppointment
	 */
	@Override
	public AppointmentDomain getAppointment(String id) {

		AppointmentDomain appointment = null;
		AndCouch req;

		try {
			req = AndCouch.get(Main.couchDBUrl + Main.dbName + "/" + id);

			JSONObject responseObject = req.json;

			try {
				String rev = responseObject.getString("_rev");
				GregorianCalendar from = stringToGreg(responseObject.getString("from"));
				GregorianCalendar to = stringToGreg(responseObject.getString("to"));
				Integer duration = responseObject.getInt("duration");
				String clientId = responseObject.getString("clientId");
				Boolean isPrivate = responseObject.getBoolean("isPrivate");
				String note = iso8859ToAscii(responseObject.getString("note"));
				Boolean completed = responseObject.getBoolean("completed");

				appointment = new AppointmentDomain(id, rev, from, to, duration, clientId, isPrivate, note, completed);

			} catch (JSONException e) {
				Log.e(tag, "get appointment failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get appointment failed: " + e1.getMessage());
			e1.printStackTrace();
		}

		return appointment;
	}

	/**
	 * @see IDBHandler#getAllAppointmentsByDate
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDate(GregorianCalendar cal) {

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/_design/servicetour/_view/getAllAppointmentsByDate";
		cal.set(GregorianCalendar.MINUTE, 0);
		cal.set(GregorianCalendar.HOUR_OF_DAY, 0);
		GregorianCalendar cal2 = (GregorianCalendar) cal.clone();
		cal2.set(GregorianCalendar.MINUTE, 59);
		cal2.set(GregorianCalendar.HOUR_OF_DAY, 23);
		ddocUrl += "?startkey=%22" + gregToString(cal) + "%22&endkey=%22" + gregToString(cal2) + "%22";

		ArrayList<AppointmentDomain> appointmentList = new ArrayList<AppointmentDomain>();

		try {
			AndCouch req = AndCouch.get(ddocUrl);
			JSONObject responseObject = req.json;

			try {
				if (responseObject.getInt("total_rows") > 0) {

					JSONArray array = responseObject.getJSONArray("rows");

					for (int i = 0; i <= array.length() - 1; i++) {

						JSONObject value = array.getJSONObject(i).getJSONObject("value");

						String id = value.getString("_id");
						String rev = value.getString("_rev");
						GregorianCalendar from = stringToGreg(value.getString("from"));
						GregorianCalendar to = stringToGreg(value.getString("to"));
						Integer duration = value.getInt("duration");
						String clientId = value.getString("clientId");
						Boolean isPrivate = value.getBoolean("isPrivate");
						Boolean completed = value.getBoolean("completed");

						appointmentList.add(new AppointmentDomain(id, rev, from, to, duration, clientId, isPrivate, completed));
					}
				}
			} catch (JSONException e) {
				Log.e(tag, "get all appointments by date failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get all appointments by date failed: " + e1.getMessage());
		}

		Log.e(tag, "get all appointments by date failed");

		return appointmentList;
	}

	/**
	 * @see IDBHandler#getAllAppointmentsByClient
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByClient(String clientId) {

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/_design/servicetour/_view/getAllAppointmentsByClient";
		ddocUrl += "?key=%22" + clientId + "%22";

		ArrayList<AppointmentDomain> appointmentList = new ArrayList<AppointmentDomain>();

		try {
			AndCouch req = AndCouch.get(ddocUrl);
			JSONObject responseObject = req.json;

			try {
				if (responseObject.getInt("total_rows") > 0) {
					JSONArray array = responseObject.getJSONArray("rows");
					for (int i = 0; i <= array.length() - 1; i++) {

						JSONObject value = array.getJSONObject(i).getJSONObject("value");

						String id = value.getString("_id");
						String rev = value.getString("_rev");
						GregorianCalendar from = stringToGreg(value.getString("from"));
						GregorianCalendar to = stringToGreg(value.getString("to"));
						Integer duration = value.getInt("duration");
						String client = value.getString("clientId");
						Boolean isPrivate = value.getBoolean("isPrivate");
						Boolean completed = value.getBoolean("completed");

						appointmentList.add(new AppointmentDomain(id, rev, from, to, duration, client, isPrivate, completed));
					}
				}
			} catch (JSONException e) {
				Log.e(tag, "get all appointments by client failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get all appointments by client failed: " + e1.getMessage());
			return null;
		}

		Log.e(tag, "get all appointments by client failed");

		return appointmentList;
	}

	/**
	 * @see IDBHandler#getAllFutureAppointmentsByClient
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllFutureAppointmentsByClient(String clientId) {

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/_design/servicetour/_view/getAllAppointmentsByClient";
		ddocUrl += "?key=%22" + clientId + "%22";

		ArrayList<AppointmentDomain> appointmentList = new ArrayList<AppointmentDomain>();

		try {
			AndCouch req = AndCouch.get(ddocUrl);
			JSONObject responseObject = req.json;

			try {
				if (responseObject.getInt("total_rows") > 0) {
					JSONArray array = responseObject.getJSONArray("rows");
					for (int i = 0; i <= array.length() - 1; i++) {

						JSONObject value = array.getJSONObject(i).getJSONObject("value");

						String id = value.getString("_id");
						String rev = value.getString("_rev");
						GregorianCalendar from = stringToGreg(value.getString("from"));
						GregorianCalendar to = stringToGreg(value.getString("to"));
						Integer duration = value.getInt("duration");
						String client = value.getString("clientId");
						Boolean isPrivate = value.getBoolean("isPrivate");
						Boolean completed = value.getBoolean("completed");

						appointmentList.add(new AppointmentDomain(id, rev, from, to, duration, client, isPrivate, completed));
					}
				}
			} catch (JSONException e) {
				Log.e(tag, "get all future appointments by client failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get all future appointments by client failed: " + e1.getMessage());
			return null;
		}

		ArrayList<AppointmentDomain> appointmentList2 = new ArrayList<AppointmentDomain>();

		for (AppointmentDomain appointment : appointmentList) {

			if (appointment.to.after(new GregorianCalendar())) {
				appointmentList2.add(appointment);
			}
		}

		return appointmentList2;
	}

	/**
	 * @see IDBHandler#getAllPastAppointmentsByClient
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllPastAppointmentsByClient(String clientId) {

		String ddocUrl = Main.couchDBUrl + Main.dbName + "/_design/servicetour/_view/getAllAppointmentsByClient";
		ddocUrl += "?key=%22" + clientId + "%22";

		ArrayList<AppointmentDomain> appointmentList = new ArrayList<AppointmentDomain>();

		try {
			AndCouch req = AndCouch.get(ddocUrl);
			JSONObject responseObject = req.json;

			try {
				if (responseObject.getInt("total_rows") > 0) {
					JSONArray array = responseObject.getJSONArray("rows");
					for (int i = 0; i <= array.length() - 1; i++) {

						JSONObject value = array.getJSONObject(i).getJSONObject("value");

						String id = value.getString("_id");
						String rev = value.getString("_rev");
						GregorianCalendar from = stringToGreg(value.getString("from"));
						GregorianCalendar to = stringToGreg(value.getString("to"));
						Integer duration = value.getInt("duration");
						String client = value.getString("clientId");
						Boolean isPrivate = value.getBoolean("isPrivate");
						Boolean completed = value.getBoolean("completed");

						appointmentList.add(new AppointmentDomain(id, rev, from, to, duration, client, isPrivate, completed));
					}
				}
			} catch (JSONException e) {
				Log.e(tag, "get all past appointments by client failed: " + e.getMessage());
				return null;
			}
		} catch (JSONException e1) {
			Log.e(tag, "get all past appointments by client failed: " + e1.getMessage());
			return null;
		}

		ArrayList<AppointmentDomain> appointmentList2 = new ArrayList<AppointmentDomain>();

		for (AppointmentDomain appointment : appointmentList) {

			if (appointment.to.before(new GregorianCalendar())) {
				appointmentList2.add(appointment);
			}
		}

		return appointmentList2;
	}

	/**
	 * @see IDBHandler#getAllAppointmentsByDateCompleted
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateCompleted(GregorianCalendar cal) {
		ArrayList<AppointmentDomain> appointments = getAllAppointmentsByDate(cal);
		ArrayList<AppointmentDomain> appointments2 = new ArrayList<AppointmentDomain>();
		for (AppointmentDomain appointment : appointments) {

			if (appointment.completed) {
				appointments2.add(appointment);
			}
		}

		return appointments2;
	}

	/**
	 * @see IDBHandler#getAllAppointmentsByDateUncompleted
	 */
	@Override
	public ArrayList<AppointmentDomain> getAllAppointmentsByDateUncompleted(GregorianCalendar cal) {
		ArrayList<AppointmentDomain> appointments = getAllAppointmentsByDate(cal);
		ArrayList<AppointmentDomain> appointments2 = new ArrayList<AppointmentDomain>();

		for (AppointmentDomain appointment : appointments) {
			if (!appointment.completed) {
				appointments2.add(appointment);
			}
		}

		return appointments2;
	}

	/**
	 * @see IDBHandler#getUUID
	 */
	public String getUUID() {

		try {
			AndCouch req = AndCouch.get(Main.couchDBUrl + "_uuids");
			JSONObject json = req.json;
			JSONArray ids;
			String id = null;
			try {
				ids = json.getJSONArray("uuids");
				id = ids.get(0).toString();
			} catch (JSONException e) {
				Log.e(tag, "get UUID failed: " + e.getMessage());
				return null;
			}

			return id;
		} catch (JSONException e1) {
			Log.e(tag, "get UUID failed: " + e1.getMessage());
		}

		return null;
	}

	/**
	 * 
	 * Converts special characters from given string in ISO-8859 characters
	 * 
	 * @param text
	 * @return modified text
	 */
	public String asciiToISO8859(String text) {

		text = text.replaceAll("�", "&Uuml;");
		text = text.replaceAll("�", "&uuml;");
		text = text.replaceAll("�", "&Ouml;");
		text = text.replaceAll("�", "&ouml;");
		text = text.replaceAll("�", "&Auml;");
		text = text.replaceAll("�", "&auml;");
		text = text.replaceAll("�", "&szlig;");

		return text;
	}

	/**
	 * 
	 * Converts special characters from given string in ascii characters
	 * 
	 * @param text
	 * @return modified text
	 */
	public String iso8859ToAscii(String text) {

		text = text.replaceAll("&Uuml;", "�");
		text = text.replaceAll("&uuml;", "�");
		text = text.replaceAll("&Ouml;", "�");
		text = text.replaceAll("&ouml;", "�");
		text = text.replaceAll("&Auml;", "�");
		text = text.replaceAll("&auml;", "�");
		text = text.replaceAll("&szlig;", "�");

		return text;
	}

	/**
	 * 
	 * Converts date in string with right format
	 * 
	 * @param cal
	 * @return date as string
	 */
	public String gregToString(GregorianCalendar cal) {

		String month = "";
		String day = "";
		String hour = "";
		String minute = "";

		if (cal.get(GregorianCalendar.MONTH) < 10) {
			month = "0" + cal.get(GregorianCalendar.MONTH);
		} else {
			month = "" + cal.get(GregorianCalendar.MONTH);
		}

		if (cal.get(GregorianCalendar.DAY_OF_MONTH) < 10) {
			day = "0" + cal.get(GregorianCalendar.DAY_OF_MONTH);
		} else {
			day = "" + cal.get(GregorianCalendar.DAY_OF_MONTH);
		}

		if (cal.get(GregorianCalendar.HOUR_OF_DAY) < 10) {
			hour = "0" + cal.get(GregorianCalendar.HOUR_OF_DAY);
		} else {
			hour = "" + cal.get(GregorianCalendar.HOUR_OF_DAY);
		}

		if (cal.get(GregorianCalendar.MINUTE) < 10) {
			minute = "0" + cal.get(GregorianCalendar.MINUTE);
		} else {
			minute = "" + cal.get(GregorianCalendar.MINUTE);
		}

		return cal.get(GregorianCalendar.YEAR) + "-" + month + "-" + day + "-" + hour + "-" + minute;
	}

	/**
	 * 
	 * Converts string in date
	 * 
	 * @param string
	 * @return date
	 */
	public GregorianCalendar stringToGreg(String string) {

		String[] date = string.split("-");
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]), Integer.valueOf(date[3]), Integer.valueOf(date[4]));
		return cal;
	}

	@Override
	public boolean syncPush(String userName, String pw, String server, String database) {
		
		String jsonString = "{" + "\"source\":\"" + Main.couchDBUrl + Main.dbName + "\",\"target\":\"http://" + userName + ":" + pw + "@" + server + "/" + database + "\"}";

		jsonString = asciiToISO8859(jsonString);

		Log.d("DOOOOOOOO: ", jsonString);
		
		try {
			AndCouch req = AndCouch.put( Main.couchDBUrl + Main.dbName + "/_replicate -d", jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "Sync Push failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "Sync Push failed");
		return false;

	}

	@Override
	public boolean syncPull(String userName, String pw, String server, String database) {

		String jsonString = "{" + "\"source\":\"http://" + userName + ":" + pw + "@" + server + "/" + database + "\",\"target\":\""  + Main.couchDBUrl + Main.dbName + "\"}";

		jsonString = asciiToISO8859(jsonString);

		Log.d("DOOOOOOOO: ", jsonString);
		
		try {
			AndCouch req = AndCouch.put( Main.couchDBUrl + Main.dbName + "/_replicate -d", jsonString);
			if (req.json.has("ok")) {
				return true;
			}
		} catch (JSONException e) {
			Log.e(tag, "Sync Pull failed: " + e.getMessage());
			return false;
		}

		Log.e(tag, "Sync Pull failed");
		return false;
		
	}
}
